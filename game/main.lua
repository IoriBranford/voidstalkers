local levity = require "levity"
love.filesystem.setRequirePath(
	"script/?.lua;"..
	love.filesystem.getRequirePath())

levity:setSystemFont("font/PCPaint Bold Small.fnt")
levity:setNextMap("game.lua")
