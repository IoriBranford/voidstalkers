<?xml version="1.0" encoding="UTF-8"?>
<tileset name="candle" tilewidth="32" tileheight="32" tilecount="8" columns="4">
 <image source="candle.png" width="128" height="64"/>
 <tile id="0">
  <animation>
   <frame tileid="0" duration="100"/>
   <frame tileid="1" duration="100"/>
   <frame tileid="2" duration="100"/>
  </animation>
 </tile>
 <tile id="4">
  <animation>
   <frame tileid="4" duration="100"/>
   <frame tileid="5" duration="100"/>
   <frame tileid="6" duration="100"/>
   <frame tileid="7" duration="100"/>
  </animation>
 </tile>
</tileset>
