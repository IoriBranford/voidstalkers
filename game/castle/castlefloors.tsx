<?xml version="1.0" encoding="UTF-8"?>
<tileset name="castlefloors" tilewidth="32" tileheight="32" tilecount="100" columns="10">
 <image source="castlefloors.png" width="320" height="320"/>
 <terraintypes>
  <terrain name="New Terrain" tile="22"/>
  <terrain name="New Terrain" tile="27"/>
  <terrain name="New Terrain" tile="72"/>
  <terrain name="New Terrain" tile="66"/>
  <terrain name="New Terrain" tile="75"/>
 </terraintypes>
 <tile id="0" terrain="0,0,0,"/>
 <tile id="4" terrain="0,0,,0"/>
 <tile id="5" terrain=",1,1,1"/>
 <tile id="9" terrain="1,,1,1"/>
 <tile id="11" terrain=",,,0"/>
 <tile id="12" terrain=",,0,0"/>
 <tile id="13" terrain=",,0,"/>
 <tile id="16" terrain=",,,1"/>
 <tile id="17" terrain=",,1,1"/>
 <tile id="18" terrain=",,1,"/>
 <tile id="21" terrain=",0,,0"/>
 <tile id="22" terrain="0,0,0,0"/>
 <tile id="23" terrain="0,,0,"/>
 <tile id="26" terrain=",1,,1"/>
 <tile id="27" terrain="1,1,1,1"/>
 <tile id="28" terrain="1,,1,"/>
 <tile id="31" terrain=",0,,"/>
 <tile id="32" terrain="0,0,,"/>
 <tile id="33" terrain="0,,,"/>
 <tile id="36" terrain=",1,,"/>
 <tile id="37" terrain="1,1,,"/>
 <tile id="38" terrain="1,,,"/>
 <tile id="40" terrain="0,,0,0"/>
 <tile id="44" terrain=",0,0,0"/>
 <tile id="45" terrain="1,1,,1"/>
 <tile id="49" terrain="1,1,1,"/>
 <tile id="50" terrain="2,2,2,"/>
 <tile id="54" terrain="2,2,,2"/>
 <tile id="55" terrain="4,3,3,3"/>
 <tile id="56" terrain="4,4,3,3"/>
 <tile id="57" terrain="3,4,3,3"/>
 <tile id="58" terrain="3,4,3,4"/>
 <tile id="61" terrain=",,,2"/>
 <tile id="62" terrain=",,2,2"/>
 <tile id="63" terrain=",,2,"/>
 <tile id="65" terrain="3,3,4,3"/>
 <tile id="66" terrain="3,3,3,3"/>
 <tile id="67" terrain="3,3,3,4"/>
 <tile id="68" terrain="4,3,4,3"/>
 <tile id="71" terrain=",2,,2"/>
 <tile id="72" terrain="2,2,2,2"/>
 <tile id="73" terrain="2,,2,"/>
 <tile id="75" terrain="4,4,4,4"/>
 <tile id="76" terrain="3,3,4,4"/>
 <tile id="81" terrain=",2,,"/>
 <tile id="82" terrain="2,2,,"/>
 <tile id="83" terrain="2,,,"/>
 <tile id="90" terrain="2,,2,2"/>
 <tile id="94" terrain=",2,2,2"/>
</tileset>
