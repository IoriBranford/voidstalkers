<?xml version="1.0" encoding="UTF-8"?>
<tileset name="enemyshot" tilewidth="16" tileheight="16" tilecount="12" columns="4">
 <tileoffset x="-8" y="8"/>
 <properties>
  <property name="commonanimation" type="int" value="0"/>
  <property name="commoncollision" type="int" value="0"/>
 </properties>
 <image source="enemyshot.png" width="64" height="48"/>
 <tile id="0">
  <objectgroup draworder="index">
   <object id="1" x="4" y="4" width="8" height="8">
    <properties>
     <property name="collidable" type="bool" value="true"/>
     <property name="sensor" type="bool" value="true"/>
    </properties>
    <ellipse/>
   </object>
  </objectgroup>
  <animation>
   <frame tileid="0" duration="16"/>
   <frame tileid="1" duration="16"/>
   <frame tileid="2" duration="16"/>
   <frame tileid="3" duration="16"/>
   <frame tileid="2" duration="16"/>
   <frame tileid="1" duration="16"/>
  </animation>
 </tile>
</tileset>
