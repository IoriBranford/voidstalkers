<?xml version="1.0" encoding="UTF-8"?>
<tileset name="playershot" tilewidth="32" tileheight="16" tilecount="7" columns="1">
 <tileoffset x="-16" y="8"/>
 <properties>
  <property name="commoncollision" value="shot"/>
 </properties>
 <image source="playershot.png" width="32" height="112"/>
 <tile id="0" type="PlayerShot">
  <properties>
   <property name="name" value="flash"/>
   <property name="nextanimtileid" value="shot"/>
  </properties>
  <animation>
   <frame tileid="0" duration="33"/>
   <frame tileid="1" duration="33"/>
  </animation>
 </tile>
 <tile id="1" type="PlayerShot">
  <properties>
   <property name="nextanimtileid" value="shot"/>
  </properties>
 </tile>
 <tile id="2" type="PlayerShot">
  <properties>
   <property name="name" value="shot"/>
  </properties>
  <objectgroup draworder="index">
   <object id="2" x="0" y="0" width="32" height="16">
    <properties>
     <property name="collidable" type="bool" value="true"/>
     <property name="sensor" type="bool" value="true"/>
    </properties>
   </object>
  </objectgroup>
  <animation>
   <frame tileid="2" duration="16"/>
   <frame tileid="3" duration="16"/>
   <frame tileid="4" duration="16"/>
   <frame tileid="3" duration="16"/>
  </animation>
 </tile>
 <tile id="3" type="PlayerShot"/>
 <tile id="4" type="PlayerShot"/>
 <tile id="5" type="PlayerShot">
  <properties>
   <property name="animenddiscard" type="bool" value="true"/>
   <property name="name" value="hit"/>
  </properties>
  <animation>
   <frame tileid="5" duration="33"/>
   <frame tileid="6" duration="33"/>
  </animation>
 </tile>
 <tile id="6" type="PlayerShot">
  <properties>
   <property name="animenddiscard" type="bool" value="true"/>
  </properties>
 </tile>
</tileset>
