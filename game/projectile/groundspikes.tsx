<?xml version="1.0" encoding="UTF-8"?>
<tileset name="groundspikes" tilewidth="64" tileheight="64" tilecount="8" columns="4">
 <tileoffset x="-32" y="8"/>
 <properties>
  <property name="commonanimation" value="earth"/>
  <property name="commoncollision" value="earth"/>
 </properties>
 <image source="groundspikes.png" width="256" height="128"/>
 <tile id="0">
  <properties>
   <property name="name" value="earth"/>
  </properties>
  <objectgroup draworder="index">
   <object id="1" x="16" y="48" width="32" height="16">
    <properties>
     <property name="collidable" type="bool" value="true"/>
     <property name="sensor" type="bool" value="true"/>
    </properties>
   </object>
  </objectgroup>
  <animation>
   <frame tileid="0" duration="33"/>
   <frame tileid="1" duration="33"/>
   <frame tileid="2" duration="33"/>
   <frame tileid="1" duration="33"/>
  </animation>
 </tile>
 <tile id="4">
  <properties>
   <property name="name" value="ice"/>
  </properties>
 </tile>
</tileset>
