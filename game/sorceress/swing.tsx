<?xml version="1.0" encoding="UTF-8"?>
<tileset name="swing" tilewidth="64" tileheight="64" tilecount="24" columns="6">
 <tileoffset x="-32" y="24"/>
 <properties>
  <property name="commonanimation" value="south"/>
  <property name="commoncollision" value="south"/>
 </properties>
 <image source="swing.png" width="384" height="256"/>
 <tile id="0" type="Player">
  <properties>
   <property name="faceangle" type="float" value="270"/>
   <property name="name" value="north"/>
  </properties>
 </tile>
 <tile id="1" type="Player">
  <properties>
   <property name="faceangle" type="float" value="270"/>
  </properties>
 </tile>
 <tile id="2" type="Player">
  <properties>
   <property name="faceangle" type="float" value="270"/>
  </properties>
 </tile>
 <tile id="3" type="Player">
  <properties>
   <property name="faceangle" type="float" value="270"/>
  </properties>
 </tile>
 <tile id="4" type="Player">
  <properties>
   <property name="faceangle" type="float" value="270"/>
  </properties>
 </tile>
 <tile id="5" type="Player">
  <properties>
   <property name="faceangle" type="float" value="270"/>
  </properties>
 </tile>
 <tile id="6" type="Player">
  <properties>
   <property name="faceangle" type="float" value="180"/>
   <property name="name" value="west"/>
  </properties>
 </tile>
 <tile id="7" type="Player">
  <properties>
   <property name="faceangle" type="float" value="180"/>
  </properties>
 </tile>
 <tile id="8" type="Player">
  <properties>
   <property name="faceangle" type="float" value="180"/>
  </properties>
 </tile>
 <tile id="9" type="Player">
  <properties>
   <property name="faceangle" type="float" value="180"/>
  </properties>
 </tile>
 <tile id="10" type="Player">
  <properties>
   <property name="faceangle" type="float" value="180"/>
  </properties>
 </tile>
 <tile id="11" type="Player">
  <properties>
   <property name="faceangle" type="float" value="180"/>
  </properties>
 </tile>
 <tile id="12" type="Player">
  <properties>
   <property name="faceangle" type="float" value="90"/>
   <property name="name" value="south"/>
  </properties>
  <objectgroup draworder="index">
   <object id="2" x="24" y="32" width="16" height="16">
    <properties>
     <property name="collidable" type="bool" value="true"/>
    </properties>
   </object>
  </objectgroup>
  <animation>
   <frame tileid="12" duration="33"/>
   <frame tileid="13" duration="33"/>
   <frame tileid="14" duration="33"/>
   <frame tileid="15" duration="33"/>
   <frame tileid="16" duration="33"/>
   <frame tileid="17" duration="33"/>
  </animation>
 </tile>
 <tile id="13" type="Player">
  <properties>
   <property name="faceangle" type="float" value="90"/>
  </properties>
 </tile>
 <tile id="14" type="Player">
  <properties>
   <property name="faceangle" type="float" value="90"/>
  </properties>
 </tile>
 <tile id="15" type="Player">
  <properties>
   <property name="faceangle" type="float" value="90"/>
  </properties>
 </tile>
 <tile id="16" type="Player">
  <properties>
   <property name="faceangle" type="float" value="90"/>
  </properties>
 </tile>
 <tile id="17" type="Player">
  <properties>
   <property name="faceangle" type="float" value="90"/>
  </properties>
 </tile>
 <tile id="18" type="Player">
  <properties>
   <property name="faceangle" type="float" value="0"/>
   <property name="name" value="east"/>
  </properties>
 </tile>
 <tile id="19" type="Player">
  <properties>
   <property name="faceangle" type="float" value="0"/>
  </properties>
 </tile>
 <tile id="20" type="Player">
  <properties>
   <property name="faceangle" type="float" value="0"/>
  </properties>
 </tile>
 <tile id="21" type="Player">
  <properties>
   <property name="faceangle" type="float" value="0"/>
  </properties>
 </tile>
 <tile id="22" type="Player">
  <properties>
   <property name="faceangle" type="float" value="0"/>
  </properties>
 </tile>
 <tile id="23" type="Player">
  <properties>
   <property name="faceangle" type="float" value="0"/>
  </properties>
 </tile>
</tileset>
