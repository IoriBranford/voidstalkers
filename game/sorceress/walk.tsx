<?xml version="1.0" encoding="UTF-8"?>
<tileset name="walk" tilewidth="64" tileheight="64" tilecount="36" columns="9">
 <tileoffset x="-32" y="24"/>
 <properties>
  <property name="commonanimation" value="walksouth"/>
  <property name="commoncollision" value="standsouth"/>
 </properties>
 <image source="walk.png" width="576" height="256"/>
 <tile id="0" type="Player">
  <properties>
   <property name="animated" type="bool" value="false"/>
   <property name="faceangle" type="float" value="270"/>
   <property name="name" value="standnorth"/>
  </properties>
 </tile>
 <tile id="1" type="Player">
  <properties>
   <property name="faceangle" type="float" value="270"/>
   <property name="name" value="walknorth"/>
  </properties>
 </tile>
 <tile id="2" type="Player">
  <properties>
   <property name="faceangle" type="float" value="270"/>
  </properties>
 </tile>
 <tile id="3" type="Player">
  <properties>
   <property name="faceangle" type="float" value="270"/>
  </properties>
 </tile>
 <tile id="4" type="Player">
  <properties>
   <property name="faceangle" type="float" value="270"/>
  </properties>
 </tile>
 <tile id="5" type="Player">
  <properties>
   <property name="faceangle" type="float" value="270"/>
  </properties>
 </tile>
 <tile id="6" type="Player">
  <properties>
   <property name="faceangle" type="float" value="270"/>
  </properties>
 </tile>
 <tile id="7" type="Player">
  <properties>
   <property name="faceangle" type="float" value="270"/>
  </properties>
 </tile>
 <tile id="8" type="Player">
  <properties>
   <property name="faceangle" type="float" value="270"/>
  </properties>
 </tile>
 <tile id="9" type="Player">
  <properties>
   <property name="animated" type="bool" value="false"/>
   <property name="faceangle" type="float" value="180"/>
   <property name="name" value="standwest"/>
  </properties>
 </tile>
 <tile id="10" type="Player">
  <properties>
   <property name="faceangle" type="float" value="180"/>
   <property name="name" value="walkwest"/>
  </properties>
 </tile>
 <tile id="11" type="Player">
  <properties>
   <property name="faceangle" type="float" value="180"/>
  </properties>
 </tile>
 <tile id="12" type="Player">
  <properties>
   <property name="faceangle" type="float" value="180"/>
  </properties>
 </tile>
 <tile id="13" type="Player">
  <properties>
   <property name="faceangle" type="float" value="180"/>
  </properties>
 </tile>
 <tile id="14" type="Player">
  <properties>
   <property name="faceangle" type="float" value="180"/>
  </properties>
 </tile>
 <tile id="15" type="Player">
  <properties>
   <property name="faceangle" type="float" value="180"/>
  </properties>
 </tile>
 <tile id="16" type="Player">
  <properties>
   <property name="faceangle" type="float" value="180"/>
  </properties>
 </tile>
 <tile id="17" type="Player">
  <properties>
   <property name="faceangle" type="float" value="180"/>
  </properties>
 </tile>
 <tile id="18" type="Player">
  <properties>
   <property name="animated" type="bool" value="false"/>
   <property name="faceangle" type="float" value="90"/>
   <property name="name" value="standsouth"/>
  </properties>
  <objectgroup draworder="index">
   <object id="1" x="24" y="32" width="16" height="16">
    <properties>
     <property name="collidable" type="bool" value="true"/>
    </properties>
   </object>
  </objectgroup>
 </tile>
 <tile id="19" type="Player">
  <properties>
   <property name="faceangle" type="float" value="90"/>
   <property name="name" value="walksouth"/>
  </properties>
  <animation>
   <frame tileid="19" duration="66"/>
   <frame tileid="20" duration="66"/>
   <frame tileid="21" duration="66"/>
   <frame tileid="22" duration="66"/>
   <frame tileid="23" duration="66"/>
   <frame tileid="24" duration="66"/>
   <frame tileid="25" duration="66"/>
   <frame tileid="26" duration="66"/>
  </animation>
 </tile>
 <tile id="20" type="Player">
  <properties>
   <property name="faceangle" type="float" value="90"/>
  </properties>
 </tile>
 <tile id="21" type="Player">
  <properties>
   <property name="faceangle" type="float" value="90"/>
  </properties>
 </tile>
 <tile id="22" type="Player">
  <properties>
   <property name="faceangle" type="float" value="90"/>
  </properties>
 </tile>
 <tile id="23" type="Player">
  <properties>
   <property name="faceangle" type="float" value="90"/>
  </properties>
 </tile>
 <tile id="24" type="Player">
  <properties>
   <property name="faceangle" type="float" value="90"/>
  </properties>
 </tile>
 <tile id="25" type="Player">
  <properties>
   <property name="faceangle" type="float" value="90"/>
  </properties>
 </tile>
 <tile id="26" type="Player">
  <properties>
   <property name="faceangle" type="float" value="90"/>
  </properties>
 </tile>
 <tile id="27" type="Player">
  <properties>
   <property name="animated" type="bool" value="false"/>
   <property name="faceangle" type="float" value="0"/>
   <property name="name" value="standeast"/>
  </properties>
 </tile>
 <tile id="28" type="Player">
  <properties>
   <property name="faceangle" type="float" value="0"/>
   <property name="name" value="walkeast"/>
  </properties>
 </tile>
 <tile id="29" type="Player">
  <properties>
   <property name="faceangle" type="float" value="0"/>
  </properties>
 </tile>
 <tile id="30" type="Player">
  <properties>
   <property name="faceangle" type="float" value="0"/>
  </properties>
 </tile>
 <tile id="31" type="Player">
  <properties>
   <property name="faceangle" type="float" value="0"/>
  </properties>
 </tile>
 <tile id="32" type="Player">
  <properties>
   <property name="faceangle" type="float" value="0"/>
  </properties>
 </tile>
 <tile id="33" type="Player">
  <properties>
   <property name="faceangle" type="float" value="0"/>
  </properties>
 </tile>
 <tile id="34" type="Player">
  <properties>
   <property name="faceangle" type="float" value="0"/>
  </properties>
 </tile>
 <tile id="35" type="Player">
  <properties>
   <property name="faceangle" type="float" value="0"/>
  </properties>
 </tile>
</tileset>
