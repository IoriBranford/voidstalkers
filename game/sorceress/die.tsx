<?xml version="1.0" encoding="UTF-8"?>
<tileset name="die" tilewidth="64" tileheight="64" tilecount="6" columns="3">
 <tileoffset x="-32" y="24"/>
 <image source="die.png" width="192" height="128"/>
 <tile id="0" type="Player">
  <animation>
   <frame tileid="0" duration="100"/>
   <frame tileid="1" duration="100"/>
   <frame tileid="2" duration="200"/>
   <frame tileid="3" duration="200"/>
   <frame tileid="2" duration="200"/>
   <frame tileid="3" duration="200"/>
   <frame tileid="2" duration="200"/>
   <frame tileid="3" duration="200"/>
   <frame tileid="2" duration="200"/>
   <frame tileid="3" duration="200"/>
   <frame tileid="4" duration="100"/>
   <frame tileid="5" duration="1100"/>
  </animation>
 </tile>
 <tile id="1" type="Player"/>
 <tile id="2" type="Player"/>
 <tile id="3" type="Player"/>
 <tile id="4" type="Player"/>
 <tile id="5" type="Player"/>
</tileset>
