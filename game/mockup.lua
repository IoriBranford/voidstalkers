return {
  version = "1.1",
  luaversion = "5.1",
  tiledversion = "1.1.6",
  orientation = "orthogonal",
  renderorder = "right-up",
  width = 15,
  height = 20,
  tilewidth = 32,
  tileheight = 32,
  nextobjectid = 30,
  properties = {},
  tilesets = {
    {
      name = "cast",
      firstgid = 1,
      filename = "sorceress/cast.tsx",
      tilewidth = 64,
      tileheight = 64,
      spacing = 0,
      margin = 0,
      image = "sorceress/cast.png",
      imagewidth = 448,
      imageheight = 256,
      tileoffset = {
        x = -32,
        y = 24
      },
      grid = {
        orientation = "orthogonal",
        width = 64,
        height = 64
      },
      properties = {
        ["commonanimation"] = "south"
      },
      terrains = {},
      tilecount = 28,
      tiles = {
        {
          id = 0,
          properties = {
            ["name"] = "north"
          }
        },
        {
          id = 7,
          properties = {
            ["name"] = "west"
          }
        },
        {
          id = 14,
          properties = {
            ["name"] = "south"
          },
          animation = {
            {
              tileid = 14,
              duration = 66
            },
            {
              tileid = 15,
              duration = 66
            },
            {
              tileid = 16,
              duration = 66
            },
            {
              tileid = 17,
              duration = 66
            },
            {
              tileid = 18,
              duration = 66
            },
            {
              tileid = 19,
              duration = 604
            },
            {
              tileid = 20,
              duration = 66
            }
          }
        },
        {
          id = 21,
          properties = {
            ["name"] = "east"
          }
        }
      }
    },
    {
      name = "die",
      firstgid = 29,
      filename = "sorceress/die.tsx",
      tilewidth = 64,
      tileheight = 64,
      spacing = 0,
      margin = 0,
      image = "sorceress/die.png",
      imagewidth = 192,
      imageheight = 128,
      tileoffset = {
        x = -32,
        y = 24
      },
      grid = {
        orientation = "orthogonal",
        width = 64,
        height = 64
      },
      properties = {},
      terrains = {},
      tilecount = 6,
      tiles = {
        {
          id = 0,
          animation = {
            {
              tileid = 0,
              duration = 100
            },
            {
              tileid = 1,
              duration = 100
            },
            {
              tileid = 2,
              duration = 200
            },
            {
              tileid = 3,
              duration = 200
            },
            {
              tileid = 2,
              duration = 200
            },
            {
              tileid = 3,
              duration = 200
            },
            {
              tileid = 2,
              duration = 200
            },
            {
              tileid = 3,
              duration = 200
            },
            {
              tileid = 2,
              duration = 200
            },
            {
              tileid = 3,
              duration = 200
            },
            {
              tileid = 4,
              duration = 100
            },
            {
              tileid = 5,
              duration = 100
            }
          }
        }
      }
    },
    {
      name = "swing",
      firstgid = 35,
      filename = "sorceress/swing.tsx",
      tilewidth = 64,
      tileheight = 64,
      spacing = 0,
      margin = 0,
      image = "sorceress/swing.png",
      imagewidth = 384,
      imageheight = 256,
      tileoffset = {
        x = -32,
        y = 24
      },
      grid = {
        orientation = "orthogonal",
        width = 64,
        height = 64
      },
      properties = {
        ["commonanimation"] = "south",
        ["commoncollision"] = "south"
      },
      terrains = {},
      tilecount = 24,
      tiles = {
        {
          id = 0,
          properties = {
            ["name"] = "north"
          }
        },
        {
          id = 6,
          properties = {
            ["name"] = "west"
          }
        },
        {
          id = 12,
          properties = {
            ["name"] = "south"
          },
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 2,
                name = "",
                type = "",
                shape = "rectangle",
                x = 24,
                y = 40,
                width = 16,
                height = 16,
                rotation = 0,
                visible = true,
                properties = {
                  ["collidable"] = true,
                  ["sensor"] = true
                }
              }
            }
          },
          animation = {
            {
              tileid = 12,
              duration = 33
            },
            {
              tileid = 13,
              duration = 33
            },
            {
              tileid = 14,
              duration = 33
            },
            {
              tileid = 15,
              duration = 33
            },
            {
              tileid = 16,
              duration = 33
            },
            {
              tileid = 17,
              duration = 33
            }
          }
        },
        {
          id = 18,
          properties = {
            ["name"] = "east"
          }
        }
      }
    },
    {
      name = "walk",
      firstgid = 59,
      filename = "sorceress/walk.tsx",
      tilewidth = 64,
      tileheight = 64,
      spacing = 0,
      margin = 0,
      image = "sorceress/walk.png",
      imagewidth = 576,
      imageheight = 256,
      tileoffset = {
        x = -32,
        y = 24
      },
      grid = {
        orientation = "orthogonal",
        width = 64,
        height = 64
      },
      properties = {
        ["commonanimation"] = "south",
        ["commoncollision"] = "south"
      },
      terrains = {},
      tilecount = 36,
      tiles = {
        {
          id = 0,
          properties = {
            ["name"] = "north"
          }
        },
        {
          id = 9,
          properties = {
            ["name"] = "west"
          }
        },
        {
          id = 18,
          properties = {
            ["name"] = "south"
          },
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 1,
                name = "",
                type = "",
                shape = "rectangle",
                x = 24,
                y = 40,
                width = 16,
                height = 16,
                rotation = 0,
                visible = true,
                properties = {
                  ["collidable"] = true,
                  ["sensor"] = true
                }
              }
            }
          },
          animation = {
            {
              tileid = 18,
              duration = 33
            },
            {
              tileid = 19,
              duration = 66
            },
            {
              tileid = 20,
              duration = 66
            },
            {
              tileid = 21,
              duration = 66
            },
            {
              tileid = 22,
              duration = 66
            },
            {
              tileid = 23,
              duration = 66
            },
            {
              tileid = 24,
              duration = 66
            },
            {
              tileid = 25,
              duration = 66
            },
            {
              tileid = 26,
              duration = 66
            }
          }
        },
        {
          id = 27,
          properties = {
            ["name"] = "east"
          }
        }
      }
    },
    {
      name = "castlefloors",
      firstgid = 95,
      filename = "castle/castlefloors.tsx",
      tilewidth = 32,
      tileheight = 32,
      spacing = 0,
      margin = 0,
      image = "castle/castlefloors.png",
      imagewidth = 320,
      imageheight = 320,
      tileoffset = {
        x = 0,
        y = 0
      },
      grid = {
        orientation = "orthogonal",
        width = 32,
        height = 32
      },
      properties = {},
      terrains = {
        {
          name = "New Terrain",
          tile = 22,
          properties = {}
        },
        {
          name = "New Terrain",
          tile = 27,
          properties = {}
        },
        {
          name = "New Terrain",
          tile = 72,
          properties = {}
        },
        {
          name = "New Terrain",
          tile = 66,
          properties = {}
        },
        {
          name = "New Terrain",
          tile = 75,
          properties = {}
        }
      },
      tilecount = 100,
      tiles = {
        {
          id = 0,
          terrain = { 0, 0, 0, -1 }
        },
        {
          id = 4,
          terrain = { 0, 0, -1, 0 }
        },
        {
          id = 5,
          terrain = { -1, 1, 1, 1 }
        },
        {
          id = 9,
          terrain = { 1, -1, 1, 1 }
        },
        {
          id = 11,
          terrain = { -1, -1, -1, 0 }
        },
        {
          id = 12,
          terrain = { -1, -1, 0, 0 }
        },
        {
          id = 13,
          terrain = { -1, -1, 0, -1 }
        },
        {
          id = 16,
          terrain = { -1, -1, -1, 1 }
        },
        {
          id = 17,
          terrain = { -1, -1, 1, 1 }
        },
        {
          id = 18,
          terrain = { -1, -1, 1, -1 }
        },
        {
          id = 21,
          terrain = { -1, 0, -1, 0 }
        },
        {
          id = 22,
          terrain = { 0, 0, 0, 0 }
        },
        {
          id = 23,
          terrain = { 0, -1, 0, -1 }
        },
        {
          id = 26,
          terrain = { -1, 1, -1, 1 }
        },
        {
          id = 27,
          terrain = { 1, 1, 1, 1 }
        },
        {
          id = 28,
          terrain = { 1, -1, 1, -1 }
        },
        {
          id = 31,
          terrain = { -1, 0, -1, -1 }
        },
        {
          id = 32,
          terrain = { 0, 0, -1, -1 }
        },
        {
          id = 33,
          terrain = { 0, -1, -1, -1 }
        },
        {
          id = 36,
          terrain = { -1, 1, -1, -1 }
        },
        {
          id = 37,
          terrain = { 1, 1, -1, -1 }
        },
        {
          id = 38,
          terrain = { 1, -1, -1, -1 }
        },
        {
          id = 40,
          terrain = { 0, -1, 0, 0 }
        },
        {
          id = 44,
          terrain = { -1, 0, 0, 0 }
        },
        {
          id = 45,
          terrain = { 1, 1, -1, 1 }
        },
        {
          id = 49,
          terrain = { 1, 1, 1, -1 }
        },
        {
          id = 50,
          terrain = { 2, 2, 2, -1 }
        },
        {
          id = 54,
          terrain = { 2, 2, -1, 2 }
        },
        {
          id = 55,
          terrain = { 4, 3, 3, 3 }
        },
        {
          id = 56,
          terrain = { 4, 4, 3, 3 }
        },
        {
          id = 57,
          terrain = { 3, 4, 3, 3 }
        },
        {
          id = 58,
          terrain = { 3, 4, 3, 4 }
        },
        {
          id = 61,
          terrain = { -1, -1, -1, 2 }
        },
        {
          id = 62,
          terrain = { -1, -1, 2, 2 }
        },
        {
          id = 63,
          terrain = { -1, -1, 2, -1 }
        },
        {
          id = 65,
          terrain = { 3, 3, 4, 3 }
        },
        {
          id = 66,
          terrain = { 3, 3, 3, 3 }
        },
        {
          id = 67,
          terrain = { 3, 3, 3, 4 }
        },
        {
          id = 68,
          terrain = { 4, 3, 4, 3 }
        },
        {
          id = 71,
          terrain = { -1, 2, -1, 2 }
        },
        {
          id = 72,
          terrain = { 2, 2, 2, 2 }
        },
        {
          id = 73,
          terrain = { 2, -1, 2, -1 }
        },
        {
          id = 75,
          terrain = { 4, 4, 4, 4 }
        },
        {
          id = 76,
          terrain = { 3, 3, 4, 4 }
        },
        {
          id = 81,
          terrain = { -1, 2, -1, -1 }
        },
        {
          id = 82,
          terrain = { 2, 2, -1, -1 }
        },
        {
          id = 83,
          terrain = { 2, -1, -1, -1 }
        },
        {
          id = 90,
          terrain = { 2, -1, 2, 2 }
        },
        {
          id = 94,
          terrain = { -1, 2, 2, 2 }
        }
      }
    },
    {
      name = "castlewalls",
      firstgid = 195,
      filename = "castle/castlewalls.tsx",
      tilewidth = 32,
      tileheight = 32,
      spacing = 0,
      margin = 0,
      image = "castle/castlewalls.png",
      imagewidth = 384,
      imageheight = 480,
      tileoffset = {
        x = 0,
        y = 0
      },
      grid = {
        orientation = "orthogonal",
        width = 32,
        height = 32
      },
      properties = {},
      terrains = {
        {
          name = "New Terrain",
          tile = 14,
          properties = {}
        }
      },
      tilecount = 180,
      tiles = {
        {
          id = 1,
          terrain = { 0, 0, -1, 0 }
        },
        {
          id = 2,
          terrain = { -1, -1, 0, 0 }
        },
        {
          id = 3,
          terrain = { 0, 0, 0, -1 }
        },
        {
          id = 13,
          terrain = { -1, 0, -1, 0 }
        },
        {
          id = 14,
          terrain = { 0, 0, 0, 0 }
        },
        {
          id = 15,
          terrain = { 0, -1, 0, -1 }
        },
        {
          id = 25,
          terrain = { -1, 0, 0, 0 }
        },
        {
          id = 26,
          terrain = { 0, 0, -1, -1 }
        },
        {
          id = 27,
          terrain = { 0, -1, 0, 0 }
        },
        {
          id = 78,
          properties = {
            ["collidable"] = true
          }
        },
        {
          id = 90,
          properties = {
            ["collidable"] = true
          }
        },
        {
          id = 91,
          properties = {
            ["collidable"] = true
          }
        },
        {
          id = 98,
          properties = {
            ["collidable"] = true
          }
        },
        {
          id = 102,
          properties = {
            ["collidable"] = true
          }
        },
        {
          id = 103,
          properties = {
            ["collidable"] = true
          }
        },
        {
          id = 108,
          properties = {
            ["collidable"] = true
          }
        },
        {
          id = 109,
          properties = {
            ["collidable"] = true
          }
        },
        {
          id = 111,
          properties = {
            ["collidable"] = true
          }
        },
        {
          id = 114,
          properties = {
            ["collidable"] = true
          }
        },
        {
          id = 115,
          properties = {
            ["collidable"] = true
          }
        }
      }
    },
    {
      name = "cabinets",
      firstgid = 375,
      filename = "castle/cabinets.tsx",
      tilewidth = 32,
      tileheight = 32,
      spacing = 0,
      margin = 0,
      image = "castle/cabinets.png",
      imagewidth = 192,
      imageheight = 416,
      tileoffset = {
        x = 0,
        y = 0
      },
      grid = {
        orientation = "orthogonal",
        width = 32,
        height = 32
      },
      properties = {},
      terrains = {},
      tilecount = 78,
      tiles = {}
    },
    {
      name = "candle",
      firstgid = 453,
      filename = "castle/candle.tsx",
      tilewidth = 32,
      tileheight = 32,
      spacing = 0,
      margin = 0,
      image = "castle/candle.png",
      imagewidth = 128,
      imageheight = 64,
      tileoffset = {
        x = 0,
        y = 0
      },
      grid = {
        orientation = "orthogonal",
        width = 32,
        height = 32
      },
      properties = {},
      terrains = {},
      tilecount = 8,
      tiles = {
        {
          id = 0,
          animation = {
            {
              tileid = 0,
              duration = 100
            },
            {
              tileid = 1,
              duration = 100
            },
            {
              tileid = 2,
              duration = 100
            }
          }
        },
        {
          id = 4,
          animation = {
            {
              tileid = 4,
              duration = 100
            },
            {
              tileid = 5,
              duration = 100
            },
            {
              tileid = 6,
              duration = 100
            },
            {
              tileid = 7,
              duration = 100
            }
          }
        }
      }
    },
    {
      name = "spider01",
      firstgid = 461,
      filename = "spider/spider01.tsx",
      tilewidth = 64,
      tileheight = 64,
      spacing = 0,
      margin = 0,
      image = "spider/spider01.png",
      imagewidth = 640,
      imageheight = 320,
      tileoffset = {
        x = -32,
        y = 32
      },
      grid = {
        orientation = "orthogonal",
        width = 64,
        height = 64
      },
      properties = {
        ["commoncollision"] = 0
      },
      terrains = {},
      tilecount = 50,
      tiles = {
        {
          id = 0,
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 1,
                name = "",
                type = "",
                shape = "ellipse",
                x = 16,
                y = 16,
                width = 32,
                height = 32,
                rotation = 0,
                visible = true,
                properties = {
                  ["collidable"] = true,
                  ["sensor"] = true
                }
              }
            }
          },
          animation = {
            {
              tileid = 0,
              duration = 100
            },
            {
              tileid = 1,
              duration = 100
            },
            {
              tileid = 2,
              duration = 100
            },
            {
              tileid = 3,
              duration = 100
            }
          }
        },
        {
          id = 4,
          animation = {
            {
              tileid = 4,
              duration = 100
            },
            {
              tileid = 5,
              duration = 100
            },
            {
              tileid = 6,
              duration = 100
            },
            {
              tileid = 7,
              duration = 100
            },
            {
              tileid = 8,
              duration = 100
            },
            {
              tileid = 9,
              duration = 100
            }
          }
        },
        {
          id = 10,
          animation = {
            {
              tileid = 10,
              duration = 100
            },
            {
              tileid = 11,
              duration = 100
            },
            {
              tileid = 12,
              duration = 100
            },
            {
              tileid = 13,
              duration = 100
            }
          }
        },
        {
          id = 14,
          animation = {
            {
              tileid = 14,
              duration = 100
            },
            {
              tileid = 15,
              duration = 100
            },
            {
              tileid = 16,
              duration = 100
            },
            {
              tileid = 17,
              duration = 100
            },
            {
              tileid = 18,
              duration = 100
            },
            {
              tileid = 19,
              duration = 100
            }
          }
        },
        {
          id = 20,
          animation = {
            {
              tileid = 20,
              duration = 100
            },
            {
              tileid = 21,
              duration = 100
            },
            {
              tileid = 22,
              duration = 100
            },
            {
              tileid = 23,
              duration = 100
            }
          }
        },
        {
          id = 24,
          animation = {
            {
              tileid = 24,
              duration = 100
            },
            {
              tileid = 25,
              duration = 100
            },
            {
              tileid = 26,
              duration = 100
            },
            {
              tileid = 27,
              duration = 100
            },
            {
              tileid = 28,
              duration = 100
            },
            {
              tileid = 29,
              duration = 100
            }
          }
        },
        {
          id = 30,
          animation = {
            {
              tileid = 30,
              duration = 100
            },
            {
              tileid = 31,
              duration = 100
            },
            {
              tileid = 32,
              duration = 100
            },
            {
              tileid = 33,
              duration = 100
            }
          }
        },
        {
          id = 34,
          animation = {
            {
              tileid = 34,
              duration = 100
            },
            {
              tileid = 35,
              duration = 100
            },
            {
              tileid = 36,
              duration = 100
            },
            {
              tileid = 37,
              duration = 100
            },
            {
              tileid = 38,
              duration = 100
            },
            {
              tileid = 39,
              duration = 100
            }
          }
        },
        {
          id = 40,
          animation = {
            {
              tileid = 40,
              duration = 33
            },
            {
              tileid = 41,
              duration = 33
            },
            {
              tileid = 42,
              duration = 33
            },
            {
              tileid = 43,
              duration = 901
            }
          }
        }
      }
    },
    {
      name = "spider02",
      firstgid = 511,
      filename = "spider/spider02.tsx",
      tilewidth = 64,
      tileheight = 64,
      spacing = 0,
      margin = 0,
      image = "spider/spider02.png",
      imagewidth = 640,
      imageheight = 320,
      tileoffset = {
        x = -32,
        y = 32
      },
      grid = {
        orientation = "orthogonal",
        width = 64,
        height = 64
      },
      properties = {
        ["commoncollision"] = 0
      },
      terrains = {},
      tilecount = 50,
      tiles = {
        {
          id = 0,
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 1,
                name = "",
                type = "",
                shape = "ellipse",
                x = 16,
                y = 16,
                width = 32,
                height = 32,
                rotation = 0,
                visible = true,
                properties = {
                  ["collidable"] = true,
                  ["sensor"] = true
                }
              }
            }
          },
          animation = {
            {
              tileid = 0,
              duration = 100
            },
            {
              tileid = 1,
              duration = 100
            },
            {
              tileid = 2,
              duration = 100
            },
            {
              tileid = 3,
              duration = 100
            }
          }
        },
        {
          id = 4,
          animation = {
            {
              tileid = 4,
              duration = 100
            },
            {
              tileid = 5,
              duration = 100
            },
            {
              tileid = 6,
              duration = 100
            },
            {
              tileid = 7,
              duration = 100
            },
            {
              tileid = 8,
              duration = 100
            },
            {
              tileid = 9,
              duration = 100
            }
          }
        },
        {
          id = 10,
          animation = {
            {
              tileid = 10,
              duration = 100
            },
            {
              tileid = 11,
              duration = 100
            },
            {
              tileid = 12,
              duration = 100
            },
            {
              tileid = 13,
              duration = 100
            }
          }
        },
        {
          id = 14,
          animation = {
            {
              tileid = 14,
              duration = 100
            },
            {
              tileid = 15,
              duration = 100
            },
            {
              tileid = 16,
              duration = 100
            },
            {
              tileid = 17,
              duration = 100
            },
            {
              tileid = 18,
              duration = 100
            },
            {
              tileid = 19,
              duration = 100
            }
          }
        },
        {
          id = 20,
          animation = {
            {
              tileid = 20,
              duration = 100
            },
            {
              tileid = 21,
              duration = 100
            },
            {
              tileid = 22,
              duration = 100
            },
            {
              tileid = 23,
              duration = 100
            }
          }
        },
        {
          id = 24,
          animation = {
            {
              tileid = 24,
              duration = 100
            },
            {
              tileid = 25,
              duration = 100
            },
            {
              tileid = 26,
              duration = 100
            },
            {
              tileid = 27,
              duration = 100
            },
            {
              tileid = 28,
              duration = 100
            },
            {
              tileid = 29,
              duration = 100
            }
          }
        },
        {
          id = 30,
          animation = {
            {
              tileid = 30,
              duration = 100
            },
            {
              tileid = 31,
              duration = 100
            },
            {
              tileid = 32,
              duration = 100
            },
            {
              tileid = 33,
              duration = 100
            }
          }
        },
        {
          id = 34,
          animation = {
            {
              tileid = 34,
              duration = 100
            },
            {
              tileid = 35,
              duration = 100
            },
            {
              tileid = 36,
              duration = 100
            },
            {
              tileid = 37,
              duration = 100
            },
            {
              tileid = 38,
              duration = 100
            },
            {
              tileid = 39,
              duration = 100
            }
          }
        },
        {
          id = 40,
          animation = {
            {
              tileid = 40,
              duration = 33
            },
            {
              tileid = 41,
              duration = 33
            },
            {
              tileid = 42,
              duration = 33
            },
            {
              tileid = 43,
              duration = 901
            }
          }
        }
      }
    },
    {
      name = "spider03",
      firstgid = 561,
      filename = "spider/spider03.tsx",
      tilewidth = 64,
      tileheight = 64,
      spacing = 0,
      margin = 0,
      image = "spider/spider03.png",
      imagewidth = 640,
      imageheight = 320,
      tileoffset = {
        x = -32,
        y = 32
      },
      grid = {
        orientation = "orthogonal",
        width = 64,
        height = 64
      },
      properties = {
        ["commoncollision"] = 0
      },
      terrains = {},
      tilecount = 50,
      tiles = {
        {
          id = 0,
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 1,
                name = "",
                type = "",
                shape = "ellipse",
                x = 16,
                y = 16,
                width = 32,
                height = 32,
                rotation = 0,
                visible = true,
                properties = {
                  ["collidable"] = true,
                  ["sensor"] = true
                }
              }
            }
          },
          animation = {
            {
              tileid = 0,
              duration = 100
            },
            {
              tileid = 1,
              duration = 100
            },
            {
              tileid = 2,
              duration = 100
            },
            {
              tileid = 3,
              duration = 100
            }
          }
        },
        {
          id = 4,
          animation = {
            {
              tileid = 4,
              duration = 100
            },
            {
              tileid = 5,
              duration = 100
            },
            {
              tileid = 6,
              duration = 100
            },
            {
              tileid = 7,
              duration = 100
            },
            {
              tileid = 8,
              duration = 100
            },
            {
              tileid = 9,
              duration = 100
            }
          }
        },
        {
          id = 10,
          animation = {
            {
              tileid = 10,
              duration = 100
            },
            {
              tileid = 11,
              duration = 100
            },
            {
              tileid = 12,
              duration = 100
            },
            {
              tileid = 13,
              duration = 100
            }
          }
        },
        {
          id = 14,
          animation = {
            {
              tileid = 14,
              duration = 100
            },
            {
              tileid = 15,
              duration = 100
            },
            {
              tileid = 16,
              duration = 100
            },
            {
              tileid = 17,
              duration = 100
            },
            {
              tileid = 18,
              duration = 100
            },
            {
              tileid = 19,
              duration = 100
            }
          }
        },
        {
          id = 20,
          animation = {
            {
              tileid = 20,
              duration = 100
            },
            {
              tileid = 21,
              duration = 100
            },
            {
              tileid = 22,
              duration = 100
            },
            {
              tileid = 23,
              duration = 100
            }
          }
        },
        {
          id = 24,
          animation = {
            {
              tileid = 24,
              duration = 100
            },
            {
              tileid = 25,
              duration = 100
            },
            {
              tileid = 26,
              duration = 100
            },
            {
              tileid = 27,
              duration = 100
            },
            {
              tileid = 28,
              duration = 100
            },
            {
              tileid = 29,
              duration = 100
            }
          }
        },
        {
          id = 30,
          animation = {
            {
              tileid = 30,
              duration = 100
            },
            {
              tileid = 31,
              duration = 100
            },
            {
              tileid = 32,
              duration = 100
            },
            {
              tileid = 33,
              duration = 100
            }
          }
        },
        {
          id = 34,
          animation = {
            {
              tileid = 34,
              duration = 100
            },
            {
              tileid = 35,
              duration = 100
            },
            {
              tileid = 36,
              duration = 100
            },
            {
              tileid = 37,
              duration = 100
            },
            {
              tileid = 38,
              duration = 100
            },
            {
              tileid = 39,
              duration = 100
            }
          }
        },
        {
          id = 40,
          animation = {
            {
              tileid = 40,
              duration = 33
            },
            {
              tileid = 41,
              duration = 33
            },
            {
              tileid = 42,
              duration = 33
            },
            {
              tileid = 43,
              duration = 901
            }
          }
        }
      }
    },
    {
      name = "dungeonex",
      firstgid = 611,
      filename = "castle/dungeonex.tsx",
      tilewidth = 32,
      tileheight = 32,
      spacing = 0,
      margin = 0,
      image = "castle/dungeonex.png",
      imagewidth = 320,
      imageheight = 320,
      tileoffset = {
        x = 0,
        y = 0
      },
      grid = {
        orientation = "orthogonal",
        width = 32,
        height = 32
      },
      properties = {},
      terrains = {},
      tilecount = 100,
      tiles = {}
    },
    {
      name = "Terrain",
      firstgid = 711,
      filename = "castle/Terrain.tsx",
      tilewidth = 32,
      tileheight = 32,
      spacing = 0,
      margin = 0,
      image = "castle/terrain.png",
      imagewidth = 1024,
      imageheight = 1024,
      tileoffset = {
        x = 0,
        y = 0
      },
      grid = {
        orientation = "orthogonal",
        width = 32,
        height = 32
      },
      properties = {},
      terrains = {
        {
          name = "Dark Dirt",
          tile = 100,
          properties = {}
        },
        {
          name = "Red Dirt",
          tile = 103,
          properties = {}
        },
        {
          name = "Black Dirt",
          tile = 106,
          properties = {}
        },
        {
          name = "Grey Dirt",
          tile = 109,
          properties = {}
        },
        {
          name = "Lava",
          tile = 112,
          properties = {}
        },
        {
          name = "Hole",
          tile = 115,
          properties = {}
        },
        {
          name = "Red Hole",
          tile = 118,
          properties = {}
        },
        {
          name = "Black Hole",
          tile = 121,
          properties = {}
        },
        {
          name = "Water",
          tile = 124,
          properties = {}
        },
        {
          name = "Full Dirt",
          tile = 537,
          properties = {}
        },
        {
          name = "Clean Dirt",
          tile = 537,
          properties = {}
        },
        {
          name = "Trans Dirt",
          tile = 97,
          properties = {}
        },
        {
          name = "Grass",
          tile = 289,
          properties = {}
        },
        {
          name = "Dark Grass",
          tile = 295,
          properties = {}
        },
        {
          name = "Short Grass",
          tile = 298,
          properties = {}
        },
        {
          name = "Long Grass",
          tile = 301,
          properties = {}
        },
        {
          name = "Wheat",
          tile = 304,
          properties = {}
        },
        {
          name = "Earth",
          tile = 676,
          properties = {}
        },
        {
          name = "Sand",
          tile = 307,
          properties = {}
        },
        {
          name = "Sand Water",
          tile = 310,
          properties = {}
        },
        {
          name = "Snow",
          tile = 499,
          properties = {}
        },
        {
          name = "Snow Water",
          tile = 662,
          properties = {}
        },
        {
          name = "Snow Ice",
          tile = 502,
          properties = {}
        },
        {
          name = "Ice",
          tile = 496,
          properties = {}
        },
        {
          name = "Brick Road",
          tile = 491,
          properties = {}
        },
        {
          name = "Sewer",
          tile = 484,
          properties = {}
        },
        {
          name = "Sewer Water",
          tile = 481,
          properties = {}
        }
      },
      tilecount = 1024,
      tiles = {
        {
          id = 1,
          terrain = { 11, 11, 11, -1 }
        },
        {
          id = 2,
          terrain = { 11, 11, -1, 11 }
        },
        {
          id = 4,
          terrain = { 0, 0, 0, -1 }
        },
        {
          id = 5,
          terrain = { 0, 0, -1, 0 }
        },
        {
          id = 7,
          terrain = { 1, 1, 1, -1 }
        },
        {
          id = 8,
          terrain = { 1, 1, -1, 1 }
        },
        {
          id = 13,
          terrain = { 3, 3, 3, -1 }
        },
        {
          id = 14,
          terrain = { 3, 3, -1, 3 }
        },
        {
          id = 16,
          terrain = { 4, 4, 4, -1 }
        },
        {
          id = 17,
          terrain = { 4, 4, -1, 4 }
        },
        {
          id = 19,
          terrain = { 5, 5, 5, -1 }
        },
        {
          id = 20,
          terrain = { 5, 5, -1, 5 }
        },
        {
          id = 22,
          terrain = { 6, 6, 6, -1 }
        },
        {
          id = 23,
          terrain = { 6, 6, -1, 6 }
        },
        {
          id = 25,
          terrain = { 7, 7, 7, -1 }
        },
        {
          id = 26,
          terrain = { 7, 7, -1, 7 }
        },
        {
          id = 28,
          terrain = { 8, 8, 8, -1 }
        },
        {
          id = 29,
          terrain = { 8, 8, -1, 8 }
        },
        {
          id = 33,
          terrain = { 11, -1, 11, 11 }
        },
        {
          id = 34,
          terrain = { -1, 11, 11, 11 }
        },
        {
          id = 36,
          terrain = { 0, -1, 0, 0 }
        },
        {
          id = 37,
          terrain = { -1, 0, 0, 0 }
        },
        {
          id = 39,
          terrain = { 1, -1, 1, 1 }
        },
        {
          id = 40,
          terrain = { -1, 1, 1, 1 }
        },
        {
          id = 45,
          terrain = { 3, -1, 3, 3 }
        },
        {
          id = 46,
          terrain = { -1, 3, 3, 3 }
        },
        {
          id = 48,
          terrain = { 4, -1, 4, 4 }
        },
        {
          id = 49,
          terrain = { -1, 4, 4, 4 }
        },
        {
          id = 51,
          terrain = { 5, -1, 5, 5 }
        },
        {
          id = 52,
          terrain = { -1, 5, 5, 5 }
        },
        {
          id = 54,
          terrain = { 6, -1, 6, 6 }
        },
        {
          id = 55,
          terrain = { -1, 6, 6, 6 }
        },
        {
          id = 57,
          terrain = { 7, -1, 7, 7 }
        },
        {
          id = 58,
          terrain = { -1, 7, 7, 7 }
        },
        {
          id = 60,
          terrain = { 8, -1, 8, 8 }
        },
        {
          id = 61,
          terrain = { -1, 8, 8, 8 }
        },
        {
          id = 64,
          terrain = { -1, -1, -1, 11 }
        },
        {
          id = 65,
          terrain = { -1, -1, 11, 11 }
        },
        {
          id = 66,
          terrain = { -1, -1, 11, -1 }
        },
        {
          id = 67,
          terrain = { -1, -1, -1, 0 }
        },
        {
          id = 68,
          terrain = { -1, -1, 0, 0 }
        },
        {
          id = 69,
          terrain = { -1, -1, 0, -1 }
        },
        {
          id = 70,
          terrain = { -1, -1, -1, 1 }
        },
        {
          id = 71,
          terrain = { -1, -1, 1, 1 }
        },
        {
          id = 72,
          terrain = { -1, -1, 1, -1 }
        },
        {
          id = 73,
          terrain = { -1, -1, -1, 2 }
        },
        {
          id = 74,
          terrain = { -1, -1, 2, 2 }
        },
        {
          id = 75,
          terrain = { -1, -1, 2, -1 }
        },
        {
          id = 76,
          terrain = { -1, -1, -1, 3 }
        },
        {
          id = 77,
          terrain = { -1, -1, 3, 3 }
        },
        {
          id = 78,
          terrain = { -1, -1, 3, -1 }
        },
        {
          id = 79,
          terrain = { -1, -1, -1, 4 }
        },
        {
          id = 80,
          terrain = { -1, -1, 4, 4 }
        },
        {
          id = 81,
          terrain = { -1, -1, 4, -1 }
        },
        {
          id = 82,
          terrain = { -1, -1, -1, 5 }
        },
        {
          id = 83,
          terrain = { -1, -1, 5, 5 }
        },
        {
          id = 84,
          terrain = { -1, -1, 5, -1 }
        },
        {
          id = 85,
          terrain = { -1, -1, -1, 6 }
        },
        {
          id = 86,
          terrain = { -1, -1, 6, 6 }
        },
        {
          id = 87,
          terrain = { -1, -1, 6, -1 }
        },
        {
          id = 88,
          terrain = { -1, -1, -1, 7 }
        },
        {
          id = 89,
          terrain = { -1, -1, 7, 7 }
        },
        {
          id = 90,
          terrain = { -1, -1, 7, -1 }
        },
        {
          id = 91,
          terrain = { -1, -1, -1, 8 }
        },
        {
          id = 92,
          terrain = { -1, -1, 8, 8 }
        },
        {
          id = 93,
          terrain = { -1, -1, 8, -1 }
        },
        {
          id = 96,
          terrain = { -1, 11, -1, 11 }
        },
        {
          id = 97,
          terrain = { 11, 11, 11, 11 }
        },
        {
          id = 98,
          terrain = { 11, -1, 11, -1 }
        },
        {
          id = 99,
          terrain = { -1, 0, -1, 0 }
        },
        {
          id = 100,
          terrain = { 0, 0, 0, 0 }
        },
        {
          id = 101,
          terrain = { 0, -1, 0, -1 }
        },
        {
          id = 102,
          terrain = { -1, 1, -1, 1 }
        },
        {
          id = 103,
          terrain = { 1, 1, 1, 1 }
        },
        {
          id = 104,
          terrain = { 1, -1, 1, -1 }
        },
        {
          id = 105,
          terrain = { -1, 2, -1, 2 }
        },
        {
          id = 106,
          terrain = { 2, 2, 2, 2 }
        },
        {
          id = 107,
          terrain = { 2, -1, 2, -1 }
        },
        {
          id = 108,
          terrain = { -1, 3, -1, 3 }
        },
        {
          id = 110,
          terrain = { 3, -1, 3, -1 }
        },
        {
          id = 111,
          terrain = { -1, 4, -1, 4 }
        },
        {
          id = 112,
          terrain = { 4, 4, 4, 4 }
        },
        {
          id = 113,
          terrain = { 4, -1, 4, -1 }
        },
        {
          id = 114,
          terrain = { -1, 5, -1, 5 }
        },
        {
          id = 115,
          terrain = { 5, 5, 5, 5 }
        },
        {
          id = 116,
          terrain = { 5, -1, 5, -1 }
        },
        {
          id = 117,
          terrain = { -1, 6, -1, 6 }
        },
        {
          id = 118,
          terrain = { 6, 6, 6, 6 }
        },
        {
          id = 119,
          terrain = { 6, -1, 6, -1 }
        },
        {
          id = 120,
          terrain = { -1, 7, -1, 7 }
        },
        {
          id = 122,
          terrain = { 7, -1, 7, -1 }
        },
        {
          id = 123,
          terrain = { -1, 8, -1, 8 }
        },
        {
          id = 125,
          terrain = { 8, -1, 8, -1 }
        },
        {
          id = 128,
          terrain = { -1, 11, -1, -1 }
        },
        {
          id = 129,
          terrain = { 11, 11, -1, -1 }
        },
        {
          id = 130,
          terrain = { 11, -1, -1, -1 }
        },
        {
          id = 131,
          terrain = { -1, 0, -1, -1 }
        },
        {
          id = 132,
          terrain = { 0, 0, -1, -1 }
        },
        {
          id = 133,
          terrain = { 0, -1, -1, -1 }
        },
        {
          id = 134,
          terrain = { -1, 1, -1, -1 }
        },
        {
          id = 135,
          terrain = { 1, 1, -1, -1 }
        },
        {
          id = 136,
          terrain = { 1, -1, -1, -1 }
        },
        {
          id = 137,
          terrain = { -1, 2, -1, -1 }
        },
        {
          id = 138,
          terrain = { 2, 2, -1, -1 }
        },
        {
          id = 139,
          terrain = { 2, -1, -1, -1 }
        },
        {
          id = 140,
          terrain = { -1, 3, -1, -1 }
        },
        {
          id = 141,
          terrain = { 3, 3, -1, -1 }
        },
        {
          id = 142,
          terrain = { 3, -1, -1, -1 }
        },
        {
          id = 143,
          terrain = { -1, 4, -1, -1 }
        },
        {
          id = 144,
          terrain = { 4, 4, -1, -1 }
        },
        {
          id = 145,
          terrain = { 4, -1, -1, -1 }
        },
        {
          id = 146,
          terrain = { -1, 5, -1, -1 }
        },
        {
          id = 147,
          terrain = { 5, 5, -1, -1 }
        },
        {
          id = 148,
          terrain = { 5, -1, -1, -1 }
        },
        {
          id = 149,
          terrain = { -1, 6, -1, -1 }
        },
        {
          id = 150,
          terrain = { 6, 6, -1, -1 }
        },
        {
          id = 151,
          terrain = { 6, -1, -1, -1 }
        },
        {
          id = 152,
          terrain = { -1, 7, -1, -1 }
        },
        {
          id = 153,
          terrain = { 7, 7, -1, -1 }
        },
        {
          id = 154,
          terrain = { 7, -1, -1, -1 }
        },
        {
          id = 155,
          terrain = { -1, 8, -1, -1 }
        },
        {
          id = 156,
          terrain = { 8, 8, -1, -1 }
        },
        {
          id = 157,
          terrain = { 8, -1, -1, -1 }
        },
        {
          id = 160,
          terrain = { 9, 9, 9, 9 }
        },
        {
          id = 161,
          terrain = { 9, 9, 9, 9 }
        },
        {
          id = 162,
          terrain = { 9, 9, 9, 9 }
        },
        {
          id = 163,
          terrain = { 0, 0, 0, 0 }
        },
        {
          id = 164,
          terrain = { 0, 0, 0, 0 }
        },
        {
          id = 165,
          terrain = { 0, 0, 0, 0 }
        },
        {
          id = 166,
          terrain = { 1, 1, 1, 1 }
        },
        {
          id = 167,
          terrain = { 1, 1, 1, 1 }
        },
        {
          id = 168,
          terrain = { 1, 1, 1, 1 }
        },
        {
          id = 169,
          terrain = { 2, 2, 2, 2 }
        },
        {
          id = 170,
          terrain = { 2, 2, 2, 2 }
        },
        {
          id = 171,
          terrain = { 2, 2, 2, 2 }
        },
        {
          id = 172,
          terrain = { 3, 3, 3, 3 }
        },
        {
          id = 173,
          terrain = { 3, 3, 3, 3 }
        },
        {
          id = 174,
          terrain = { 3, 3, 3, 3 }
        },
        {
          id = 175,
          terrain = { 4, 4, 4, 4 }
        },
        {
          id = 176,
          terrain = { 4, 4, 4, 4 }
        },
        {
          id = 177,
          terrain = { 4, 4, 4, 4 }
        },
        {
          id = 184,
          terrain = { 7, 7, 7, 7 }
        },
        {
          id = 185,
          terrain = { 7, 7, 7, 7 }
        },
        {
          id = 186,
          terrain = { 7, 7, 7, 7 }
        },
        {
          id = 187,
          terrain = { 8, 8, 8, 8 }
        },
        {
          id = 188,
          terrain = { 8, 8, 8, 8 }
        },
        {
          id = 189,
          terrain = { 8, 8, 8, 8 }
        },
        {
          id = 193,
          terrain = { 12, 12, 12, -1 }
        },
        {
          id = 194,
          terrain = { 12, 12, -1, 12 }
        },
        {
          id = 196,
          terrain = { 12, 12, 12, -1 }
        },
        {
          id = 197,
          terrain = { 12, 12, -1, 12 }
        },
        {
          id = 199,
          terrain = { 13, 13, 13, -1 }
        },
        {
          id = 200,
          terrain = { 13, 13, -1, 13 }
        },
        {
          id = 202,
          terrain = { 14, 14, 14, -1 }
        },
        {
          id = 203,
          terrain = { 14, 14, -1, 14 }
        },
        {
          id = 205,
          terrain = { 15, 15, 15, -1 }
        },
        {
          id = 206,
          terrain = { 15, 15, -1, 15 }
        },
        {
          id = 208,
          terrain = { 16, 16, 16, -1 }
        },
        {
          id = 209,
          terrain = { 16, 16, -1, 16 }
        },
        {
          id = 211,
          terrain = { 18, 18, 18, -1 }
        },
        {
          id = 212,
          terrain = { 18, 18, -1, 18 }
        },
        {
          id = 214,
          terrain = { 19, 19, 19, -1 }
        },
        {
          id = 215,
          terrain = { 19, 19, -1, 19 }
        },
        {
          id = 225,
          terrain = { 12, -1, 12, 12 }
        },
        {
          id = 226,
          terrain = { -1, 12, 12, 12 }
        },
        {
          id = 228,
          terrain = { 12, -1, 12, 12 }
        },
        {
          id = 229,
          terrain = { -1, 12, 12, 12 }
        },
        {
          id = 231,
          terrain = { 13, -1, 13, 13 }
        },
        {
          id = 232,
          terrain = { -1, 13, 13, 13 }
        },
        {
          id = 234,
          terrain = { 14, -1, 14, 14 }
        },
        {
          id = 235,
          terrain = { -1, 14, 14, 14 }
        },
        {
          id = 237,
          terrain = { 15, -1, 15, 15 }
        },
        {
          id = 238,
          terrain = { -1, 15, 15, 15 }
        },
        {
          id = 240,
          terrain = { 16, -1, 16, 16 }
        },
        {
          id = 241,
          terrain = { -1, 16, 16, 16 }
        },
        {
          id = 243,
          terrain = { 18, -1, 18, 18 }
        },
        {
          id = 244,
          terrain = { -1, 18, 18, 18 }
        },
        {
          id = 246,
          terrain = { 19, -1, 19, 19 }
        },
        {
          id = 247,
          terrain = { -1, 19, 19, 19 }
        },
        {
          id = 256,
          terrain = { -1, -1, -1, 12 }
        },
        {
          id = 257,
          terrain = { -1, -1, 12, 12 }
        },
        {
          id = 258,
          terrain = { -1, -1, 12, -1 }
        },
        {
          id = 259,
          terrain = { -1, -1, -1, 12 }
        },
        {
          id = 260,
          terrain = { -1, -1, 12, 12 }
        },
        {
          id = 261,
          terrain = { -1, -1, 12, -1 }
        },
        {
          id = 262,
          terrain = { -1, -1, -1, 13 }
        },
        {
          id = 263,
          terrain = { -1, -1, 13, 13 }
        },
        {
          id = 264,
          terrain = { -1, -1, 13, -1 }
        },
        {
          id = 265,
          terrain = { -1, -1, -1, 14 }
        },
        {
          id = 266,
          terrain = { -1, -1, 14, 14 }
        },
        {
          id = 267,
          terrain = { -1, -1, 14, -1 }
        },
        {
          id = 268,
          terrain = { -1, -1, -1, 15 }
        },
        {
          id = 269,
          terrain = { -1, -1, 15, 15 }
        },
        {
          id = 270,
          terrain = { -1, -1, 15, -1 }
        },
        {
          id = 271,
          terrain = { -1, -1, -1, 16 }
        },
        {
          id = 272,
          terrain = { -1, -1, 16, 16 }
        },
        {
          id = 273,
          terrain = { -1, -1, 16, -1 }
        },
        {
          id = 274,
          terrain = { -1, -1, -1, 18 }
        },
        {
          id = 275,
          terrain = { -1, -1, 18, 18 }
        },
        {
          id = 276,
          terrain = { -1, -1, 18, -1 }
        },
        {
          id = 277,
          terrain = { -1, -1, -1, 19 }
        },
        {
          id = 278,
          terrain = { -1, -1, 19, 19 }
        },
        {
          id = 279,
          terrain = { -1, -1, 19, -1 }
        },
        {
          id = 288,
          terrain = { -1, 12, -1, 12 }
        },
        {
          id = 290,
          terrain = { 12, -1, 12, -1 }
        },
        {
          id = 291,
          terrain = { -1, 12, -1, 12 }
        },
        {
          id = 292,
          terrain = { 12, 12, 12, 12 }
        },
        {
          id = 293,
          terrain = { 12, -1, 12, -1 }
        },
        {
          id = 294,
          terrain = { -1, 13, -1, 13 }
        },
        {
          id = 295,
          terrain = { 13, 13, 13, 13 }
        },
        {
          id = 296,
          terrain = { 13, -1, 13, -1 }
        },
        {
          id = 297,
          terrain = { -1, 14, -1, 14 }
        },
        {
          id = 298,
          terrain = { 14, 14, 14, 14 }
        },
        {
          id = 299,
          terrain = { 14, -1, 14, -1 }
        },
        {
          id = 300,
          terrain = { -1, 15, -1, 15 }
        },
        {
          id = 301,
          terrain = { 15, 15, 15, 15 }
        },
        {
          id = 302,
          terrain = { 15, -1, 15, -1 }
        },
        {
          id = 303,
          terrain = { -1, 16, -1, 16 }
        },
        {
          id = 304,
          terrain = { 16, 16, 16, 16 }
        },
        {
          id = 305,
          terrain = { 16, -1, 16, -1 }
        },
        {
          id = 306,
          terrain = { -1, 18, -1, 18 }
        },
        {
          id = 307,
          terrain = { 18, 18, 18, 18 }
        },
        {
          id = 308,
          terrain = { 18, -1, 18, -1 }
        },
        {
          id = 309,
          terrain = { -1, 19, -1, 19 }
        },
        {
          id = 310,
          terrain = { 19, 19, 19, 19 }
        },
        {
          id = 311,
          terrain = { 19, -1, 19, -1 }
        },
        {
          id = 320,
          terrain = { -1, 12, -1, -1 }
        },
        {
          id = 321,
          terrain = { 12, 12, -1, -1 }
        },
        {
          id = 322,
          terrain = { 12, -1, -1, -1 }
        },
        {
          id = 323,
          terrain = { -1, 12, -1, -1 }
        },
        {
          id = 324,
          terrain = { 12, 12, -1, -1 }
        },
        {
          id = 325,
          terrain = { 12, -1, -1, -1 }
        },
        {
          id = 326,
          terrain = { -1, 13, -1, -1 }
        },
        {
          id = 327,
          terrain = { 13, 13, -1, -1 }
        },
        {
          id = 328,
          terrain = { 13, -1, -1, -1 }
        },
        {
          id = 329,
          terrain = { -1, 14, -1, -1 }
        },
        {
          id = 330,
          terrain = { 14, 14, -1, -1 }
        },
        {
          id = 331,
          terrain = { 14, -1, -1, -1 }
        },
        {
          id = 332,
          terrain = { -1, 15, -1, -1 }
        },
        {
          id = 333,
          terrain = { 15, 15, -1, -1 }
        },
        {
          id = 334,
          terrain = { 15, -1, -1, -1 }
        },
        {
          id = 335,
          terrain = { -1, 16, -1, -1 }
        },
        {
          id = 336,
          terrain = { 16, 16, -1, -1 }
        },
        {
          id = 337,
          terrain = { 16, -1, -1, -1 }
        },
        {
          id = 338,
          terrain = { -1, 18, -1, -1 }
        },
        {
          id = 339,
          terrain = { 18, 18, -1, -1 }
        },
        {
          id = 340,
          terrain = { 18, -1, -1, -1 }
        },
        {
          id = 341,
          terrain = { -1, 19, -1, -1 }
        },
        {
          id = 342,
          terrain = { 19, 19, -1, -1 }
        },
        {
          id = 343,
          terrain = { 19, -1, -1, -1 }
        },
        {
          id = 352,
          terrain = { 12, 12, 12, 12 }
        },
        {
          id = 353,
          terrain = { 12, 12, 12, 12 }
        },
        {
          id = 354,
          terrain = { 12, 12, 12, 12 }
        },
        {
          id = 358,
          terrain = { 13, 13, 13, 13 }
        },
        {
          id = 359,
          terrain = { 13, 13, 13, 13 }
        },
        {
          id = 360,
          terrain = { 13, 13, 13, 13 }
        },
        {
          id = 370,
          terrain = { 18, 18, 18, 18 }
        },
        {
          id = 371,
          terrain = { 18, 18, 18, 18 }
        },
        {
          id = 372,
          terrain = { 18, 18, 18, 18 }
        },
        {
          id = 385,
          terrain = { 26, 26, 26, -1 }
        },
        {
          id = 386,
          terrain = { 26, 26, -1, 26 }
        },
        {
          id = 388,
          terrain = { 25, 25, 25, -1 }
        },
        {
          id = 389,
          terrain = { 25, 25, -1, 25 }
        },
        {
          id = 395,
          terrain = { 24, 24, 24, -1 }
        },
        {
          id = 396,
          terrain = { 24, 24, -1, -1 }
        },
        {
          id = 397,
          terrain = { 24, 24, -1, 24 }
        },
        {
          id = 398,
          terrain = { 24, 24, 24, 24 }
        },
        {
          id = 400,
          terrain = { 23, 23, 23, -1 }
        },
        {
          id = 401,
          terrain = { 23, 23, -1, 23 }
        },
        {
          id = 403,
          terrain = { 20, 20, 20, -1 }
        },
        {
          id = 404,
          terrain = { 20, 20, -1, 20 }
        },
        {
          id = 406,
          terrain = { 22, 22, 22, -1 }
        },
        {
          id = 407,
          terrain = { 22, 22, -1, 22 }
        },
        {
          id = 417,
          terrain = { 26, -1, 26, 26 }
        },
        {
          id = 418,
          terrain = { -1, 26, 26, 26 }
        },
        {
          id = 420,
          terrain = { 25, -1, 25, 25 }
        },
        {
          id = 421,
          terrain = { -1, 25, 25, 25 }
        },
        {
          id = 427,
          terrain = { 24, -1, 24, -1 }
        },
        {
          id = 429,
          terrain = { -1, 24, -1, 24 }
        },
        {
          id = 430,
          terrain = { 24, 24, 24, 24 }
        },
        {
          id = 432,
          terrain = { 23, -1, 23, 23 }
        },
        {
          id = 433,
          terrain = { -1, 23, 23, 23 }
        },
        {
          id = 435,
          terrain = { 20, -1, 20, 20 }
        },
        {
          id = 436,
          terrain = { -1, 20, 20, 20 }
        },
        {
          id = 438,
          terrain = { 22, -1, 22, 22 }
        },
        {
          id = 439,
          terrain = { -1, 22, 22, 22 }
        },
        {
          id = 448,
          terrain = { -1, -1, -1, 26 }
        },
        {
          id = 449,
          terrain = { -1, -1, 26, 26 }
        },
        {
          id = 450,
          terrain = { -1, -1, 26, -1 }
        },
        {
          id = 451,
          terrain = { -1, -1, -1, 25 }
        },
        {
          id = 452,
          terrain = { -1, -1, 25, 25 }
        },
        {
          id = 453,
          terrain = { -1, -1, 25, -1 }
        },
        {
          id = 459,
          terrain = { 24, -1, 24, 24 }
        },
        {
          id = 460,
          terrain = { -1, -1, 24, 24 }
        },
        {
          id = 461,
          terrain = { -1, 24, 24, 24 }
        },
        {
          id = 462,
          terrain = { 24, 24, 24, 24 }
        },
        {
          id = 463,
          terrain = { -1, -1, -1, 23 }
        },
        {
          id = 464,
          terrain = { -1, -1, 23, 23 }
        },
        {
          id = 465,
          terrain = { -1, -1, 23, -1 }
        },
        {
          id = 466,
          terrain = { -1, -1, -1, 20 }
        },
        {
          id = 467,
          terrain = { -1, -1, 20, 20 }
        },
        {
          id = 468,
          terrain = { -1, -1, 20, -1 }
        },
        {
          id = 469,
          terrain = { -1, -1, -1, 22 }
        },
        {
          id = 470,
          terrain = { -1, -1, 22, 22 }
        },
        {
          id = 471,
          terrain = { -1, -1, 22, -1 }
        },
        {
          id = 480,
          terrain = { -1, 26, -1, 26 }
        },
        {
          id = 482,
          terrain = { 26, -1, 26, -1 }
        },
        {
          id = 483,
          terrain = { -1, 25, -1, 25 }
        },
        {
          id = 484,
          terrain = { 25, 25, 25, 25 }
        },
        {
          id = 485,
          terrain = { 25, -1, 25, -1 }
        },
        {
          id = 491,
          terrain = { 24, 24, 24, 24 }
        },
        {
          id = 492,
          terrain = { 24, 24, 24, 24 }
        },
        {
          id = 493,
          terrain = { 24, 24, 24, 24 }
        },
        {
          id = 494,
          terrain = { 24, 24, 24, 24 }
        },
        {
          id = 495,
          terrain = { -1, 23, -1, 23 }
        },
        {
          id = 496,
          terrain = { 23, 23, 23, 23 }
        },
        {
          id = 497,
          terrain = { 23, -1, 23, -1 }
        },
        {
          id = 498,
          terrain = { -1, 20, -1, 20 }
        },
        {
          id = 499,
          terrain = { 20, 20, 20, 20 }
        },
        {
          id = 500,
          terrain = { 20, -1, 20, -1 }
        },
        {
          id = 501,
          terrain = { -1, 22, -1, 22 }
        },
        {
          id = 502,
          terrain = { 22, 22, 22, 22 }
        },
        {
          id = 503,
          terrain = { 22, -1, 22, -1 }
        },
        {
          id = 504,
          terrain = { 9, 9, 9, 10 }
        },
        {
          id = 505,
          terrain = { 9, 9, 10, 10 }
        },
        {
          id = 506,
          terrain = { 9, 9, 10, 9 }
        },
        {
          id = 507,
          terrain = { 10, 10, 10, 9 }
        },
        {
          id = 508,
          terrain = { 10, 10, 9, 10 }
        },
        {
          id = 512,
          terrain = { -1, 26, -1, -1 }
        },
        {
          id = 513,
          terrain = { 26, 26, -1, -1 }
        },
        {
          id = 514,
          terrain = { 26, -1, -1, -1 }
        },
        {
          id = 515,
          terrain = { -1, 25, -1, -1 }
        },
        {
          id = 516,
          terrain = { 25, 25, -1, -1 }
        },
        {
          id = 517,
          terrain = { 25, -1, -1, -1 }
        },
        {
          id = 524,
          terrain = { -1, -1, -1, 24 }
        },
        {
          id = 525,
          terrain = { -1, -1, 24, -1 }
        },
        {
          id = 527,
          terrain = { -1, 23, -1, -1 }
        },
        {
          id = 528,
          terrain = { 23, 23, -1, -1 }
        },
        {
          id = 529,
          terrain = { 23, -1, -1, -1 }
        },
        {
          id = 530,
          terrain = { -1, 20, -1, -1 }
        },
        {
          id = 531,
          terrain = { 20, 20, -1, -1 }
        },
        {
          id = 532,
          terrain = { 20, -1, -1, -1 }
        },
        {
          id = 533,
          terrain = { -1, 22, -1, -1 }
        },
        {
          id = 534,
          terrain = { 22, 22, -1, -1 }
        },
        {
          id = 535,
          terrain = { 22, -1, -1, -1 }
        },
        {
          id = 536,
          terrain = { 9, 10, 9, 10 }
        },
        {
          id = 537,
          terrain = { 10, 10, 10, 10 }
        },
        {
          id = 538,
          terrain = { 10, 9, 10, 9 }
        },
        {
          id = 539,
          terrain = { 10, 9, 10, 10 }
        },
        {
          id = 540,
          terrain = { 9, 10, 10, 10 }
        },
        {
          id = 544,
          terrain = { 26, 26, 26, 26 }
        },
        {
          id = 545,
          terrain = { 26, 26, 26, 26 }
        },
        {
          id = 546,
          terrain = { 26, 26, 26, 26 }
        },
        {
          id = 547,
          terrain = { 25, 25, 25, 25 }
        },
        {
          id = 548,
          terrain = { 25, 25, 25, 25 }
        },
        {
          id = 549,
          terrain = { 25, 25, 25, 25 }
        },
        {
          id = 556,
          terrain = { -1, 24, -1, -1 }
        },
        {
          id = 557,
          terrain = { 24, -1, -1, -1 }
        },
        {
          id = 559,
          terrain = { 23, 23, 23, 23 }
        },
        {
          id = 560,
          terrain = { 23, 23, 23, 23 }
        },
        {
          id = 561,
          terrain = { 23, 23, 23, 23 }
        },
        {
          id = 562,
          terrain = { 20, 20, 20, 20 }
        },
        {
          id = 563,
          terrain = { 20, 20, 20, 20 }
        },
        {
          id = 564,
          terrain = { 20, 20, 20, 20 }
        },
        {
          id = 566,
          terrain = { 21, 21, 21, -1 }
        },
        {
          id = 567,
          terrain = { 21, 21, -1, 21 }
        },
        {
          id = 568,
          terrain = { 9, 10, 9, 9 }
        },
        {
          id = 569,
          terrain = { 10, 10, 9, 9 }
        },
        {
          id = 570,
          terrain = { 10, 9, 9, 9 }
        },
        {
          id = 571,
          terrain = { 10, 10, 10, 10 }
        },
        {
          id = 572,
          terrain = { 10, 10, 10, 10 }
        },
        {
          id = 580,
          terrain = { 17, 17, 17, -1 }
        },
        {
          id = 581,
          terrain = { 17, 17, -1, 17 }
        },
        {
          id = 598,
          terrain = { 21, -1, 21, 21 }
        },
        {
          id = 599,
          terrain = { -1, 21, 21, 21 }
        },
        {
          id = 600,
          terrain = { 9, 9, 9, 9 }
        },
        {
          id = 601,
          terrain = { 9, 9, 9, 9 }
        },
        {
          id = 602,
          terrain = { 9, 9, 9, 9 }
        },
        {
          id = 603,
          terrain = { 9, 9, 9, 9 }
        },
        {
          id = 604,
          terrain = { 9, 9, 9, 9 }
        },
        {
          id = 612,
          terrain = { 17, -1, 17, 17 }
        },
        {
          id = 613,
          terrain = { -1, 17, 17, 17 }
        },
        {
          id = 629,
          terrain = { -1, -1, -1, 21 }
        },
        {
          id = 630,
          terrain = { -1, -1, 21, 21 }
        },
        {
          id = 631,
          terrain = { -1, -1, 21, -1 }
        },
        {
          id = 643,
          terrain = { -1, -1, -1, 17 }
        },
        {
          id = 644,
          terrain = { -1, -1, 17, 17 }
        },
        {
          id = 645,
          terrain = { -1, -1, 17, -1 }
        },
        {
          id = 661,
          terrain = { -1, 21, -1, 21 }
        },
        {
          id = 662,
          terrain = { 21, 21, 21, 21 }
        },
        {
          id = 663,
          terrain = { 21, -1, 21, -1 }
        },
        {
          id = 675,
          terrain = { -1, 17, -1, 17 }
        },
        {
          id = 676,
          terrain = { 17, 17, 17, 17 }
        },
        {
          id = 677,
          terrain = { 17, -1, 17, -1 }
        },
        {
          id = 693,
          terrain = { -1, 21, -1, -1 }
        },
        {
          id = 694,
          terrain = { 21, 21, -1, -1 }
        },
        {
          id = 695,
          terrain = { 21, -1, -1, -1 }
        },
        {
          id = 707,
          terrain = { -1, 17, -1, -1 }
        },
        {
          id = 708,
          terrain = { 17, 17, -1, -1 }
        },
        {
          id = 709,
          terrain = { 17, -1, -1, -1 }
        }
      }
    },
    {
      name = "groundspikes",
      firstgid = 1735,
      filename = "projectile/groundspikes.tsx",
      tilewidth = 64,
      tileheight = 64,
      spacing = 0,
      margin = 0,
      image = "projectile/groundspikes.png",
      imagewidth = 256,
      imageheight = 128,
      tileoffset = {
        x = -32,
        y = 8
      },
      grid = {
        orientation = "orthogonal",
        width = 64,
        height = 64
      },
      properties = {
        ["commonanimation"] = "earth",
        ["commoncollision"] = "earth"
      },
      terrains = {},
      tilecount = 8,
      tiles = {
        {
          id = 0,
          properties = {
            ["name"] = "earth"
          },
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 1,
                name = "",
                type = "",
                shape = "rectangle",
                x = 16,
                y = 48,
                width = 32,
                height = 16,
                rotation = 0,
                visible = true,
                properties = {
                  ["collidable"] = true,
                  ["sensor"] = true
                }
              }
            }
          },
          animation = {
            {
              tileid = 0,
              duration = 33
            },
            {
              tileid = 1,
              duration = 33
            },
            {
              tileid = 2,
              duration = 33
            },
            {
              tileid = 1,
              duration = 33
            }
          }
        },
        {
          id = 4,
          properties = {
            ["name"] = "ice"
          }
        }
      }
    },
    {
      name = "playershot",
      firstgid = 1743,
      filename = "projectile/playershot.tsx",
      tilewidth = 32,
      tileheight = 16,
      spacing = 0,
      margin = 0,
      image = "projectile/playershot.png",
      imagewidth = 32,
      imageheight = 80,
      tileoffset = {
        x = -16,
        y = 8
      },
      grid = {
        orientation = "orthogonal",
        width = 32,
        height = 16
      },
      properties = {},
      terrains = {},
      tilecount = 5,
      tiles = {
        {
          id = 0,
          animation = {
            {
              tileid = 0,
              duration = 33
            },
            {
              tileid = 1,
              duration = 33
            }
          }
        },
        {
          id = 2,
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 2,
                name = "",
                type = "",
                shape = "rectangle",
                x = 0,
                y = 0,
                width = 32,
                height = 16,
                rotation = 0,
                visible = true,
                properties = {
                  ["collidable"] = true,
                  ["sensor"] = true
                }
              }
            }
          },
          animation = {
            {
              tileid = 2,
              duration = 16
            },
            {
              tileid = 3,
              duration = 16
            },
            {
              tileid = 4,
              duration = 16
            },
            {
              tileid = 3,
              duration = 16
            }
          }
        }
      }
    },
    {
      name = "enemyshot",
      firstgid = 1748,
      filename = "projectile/enemyshot.tsx",
      tilewidth = 16,
      tileheight = 16,
      spacing = 0,
      margin = 0,
      image = "projectile/enemyshot.png",
      imagewidth = 64,
      imageheight = 48,
      tileoffset = {
        x = -8,
        y = 8
      },
      grid = {
        orientation = "orthogonal",
        width = 16,
        height = 16
      },
      properties = {
        ["commonanimation"] = 0,
        ["commoncollision"] = 0
      },
      terrains = {},
      tilecount = 12,
      tiles = {
        {
          id = 0,
          objectGroup = {
            type = "objectgroup",
            name = "",
            visible = true,
            opacity = 1,
            offsetx = 0,
            offsety = 0,
            draworder = "index",
            properties = {},
            objects = {
              {
                id = 1,
                name = "",
                type = "",
                shape = "ellipse",
                x = 4,
                y = 4,
                width = 8,
                height = 8,
                rotation = 0,
                visible = true,
                properties = {
                  ["collidable"] = true,
                  ["sensor"] = true
                }
              }
            }
          },
          animation = {
            {
              tileid = 0,
              duration = 16
            },
            {
              tileid = 1,
              duration = 16
            },
            {
              tileid = 2,
              duration = 16
            },
            {
              tileid = 3,
              duration = 16
            },
            {
              tileid = 2,
              duration = 16
            },
            {
              tileid = 1,
              duration = 16
            }
          }
        }
      }
    }
  },
  layers = {
    {
      type = "tilelayer",
      name = "castlefloors",
      x = 0,
      y = 0,
      width = 15,
      height = 20,
      visible = true,
      opacity = 1,
      offsetx = 0,
      offsety = 0,
      properties = {},
      encoding = "base64",
      compression = "zlib",
      data = "eJxbxcDAsGoI4iwgziYB5yDpLQHiUhJw2ajeUb3DVO9QwwD+66EJ"
    },
    {
      type = "tilelayer",
      name = "castlewalls",
      x = 0,
      y = 0,
      width = 15,
      height = 20,
      visible = true,
      opacity = 1,
      offsetx = 0,
      offsety = 0,
      properties = {},
      encoding = "base64",
      compression = "zlib",
      data = "eJzTYmRgEAdiCSCWRMJSBMQ1gVgLiJWBWAVNjRoBcZhefSA2AGJVJGxEQBymlxwwqndU72DUa0amXlOgvotAWpCRdAzSNxQxAJtEMdo="
    },
    {
      type = "tilelayer",
      name = "decoration",
      x = 0,
      y = 0,
      width = 15,
      height = 20,
      visible = true,
      opacity = 1,
      offsetx = 0,
      offsety = 0,
      properties = {},
      encoding = "base64",
      compression = "zlib",
      data = "eJxjYGBgOMrIgBWQIl4OFKsA4kqoXCeU7gLS3UDcg8MsEKgFytUBcT1UTT+UngCkJwLxJDx6m4FyLUDcClUzFUpPA9LTgXgGHr2EwDYm/PKTCMiPJDBUwqKDCHcSindK7KGG2SAwb4iEN7UBADWVEqA="
    },
    {
      type = "tilelayer",
      name = "void",
      x = 0,
      y = 0,
      width = 15,
      height = 20,
      visible = true,
      opacity = 1,
      offsetx = 0,
      offsety = 0,
      properties = {},
      encoding = "base64",
      compression = "zlib",
      data = "eJzVU0EKwCAM6/Qj+8nYT/aT6k+3n8yCQiyMaXZaIVjENGnVHEVyQaqwXOuKSLCP+RlEjviOK/R1rcZeMBKb8zfLVeDPcnEeDLdpKtEvOyt/h4xnhtv6VFJXYWU8M7qo+eVdWS4F6wAWNyf0r7HvRd3Z5DRH/t4T/hg3RXs+oQ=="
    },
    {
      type = "objectgroup",
      name = "collision",
      visible = false,
      opacity = 1,
      offsetx = 0,
      offsety = 0,
      draworder = "topdown",
      properties = {
        ["static"] = true
      },
      objects = {
        {
          id = 2,
          name = "",
          type = "",
          shape = "polyline",
          x = 80,
          y = 88,
          width = 0,
          height = 0,
          rotation = 0,
          visible = true,
          polyline = {
            { x = 0, y = 0 },
            { x = 16, y = 16 },
            { x = 112, y = 16 },
            { x = 128, y = 0 },
            { x = 144, y = 16 },
            { x = 176, y = 16 },
            { x = 192, y = 0 },
            { x = 208, y = 16 },
            { x = 304, y = 16 },
            { x = 320, y = 0 }
          },
          properties = {
            ["collidable"] = true
          }
        }
      }
    },
    {
      type = "objectgroup",
      name = "objects",
      visible = true,
      opacity = 1,
      offsetx = 0,
      offsety = 0,
      draworder = "topdown",
      properties = {},
      objects = {
        {
          id = 1,
          name = "player",
          type = "",
          shape = "rectangle",
          x = 240,
          y = 304,
          width = 64,
          height = 64,
          rotation = 0,
          gid = 47,
          visible = true,
          properties = {}
        },
        {
          id = 5,
          name = "",
          type = "",
          shape = "rectangle",
          x = 376,
          y = 192,
          width = 64,
          height = 64,
          rotation = 0,
          gid = 474,
          visible = true,
          properties = {
            ["animated"] = false
          }
        },
        {
          id = 6,
          name = "",
          type = "",
          shape = "rectangle",
          x = 72,
          y = 280,
          width = 64,
          height = 64,
          rotation = 0,
          gid = 573,
          visible = true,
          properties = {
            ["animated"] = false
          }
        },
        {
          id = 7,
          name = "",
          type = "",
          shape = "rectangle",
          x = 376,
          y = 304,
          width = 64,
          height = 64,
          rotation = 0,
          gid = 474,
          visible = true,
          properties = {
            ["animated"] = false
          }
        },
        {
          id = 8,
          name = "",
          type = "",
          shape = "rectangle",
          x = 40,
          y = 232,
          width = 64,
          height = 64,
          rotation = 0,
          gid = 494,
          visible = true,
          properties = {
            ["animated"] = false
          }
        },
        {
          id = 9,
          name = "",
          type = "",
          shape = "rectangle",
          x = 152,
          y = 448,
          width = 64,
          height = 64,
          rotation = 0,
          gid = 534,
          visible = true,
          properties = {
            ["animated"] = false
          }
        },
        {
          id = 10,
          name = "",
          type = "",
          shape = "rectangle",
          x = 240,
          y = 336,
          width = 32,
          height = 16,
          rotation = -270,
          gid = 1743,
          visible = true,
          properties = {
            ["animated"] = false
          }
        },
        {
          id = 11,
          name = "",
          type = "",
          shape = "rectangle",
          x = 240,
          y = 432,
          width = 32,
          height = 16,
          rotation = -270,
          gid = 1745,
          visible = true,
          properties = {
            ["animated"] = false
          }
        },
        {
          id = 12,
          name = "",
          type = "",
          shape = "rectangle",
          x = 240,
          y = 400,
          width = 32,
          height = 16,
          rotation = -270,
          gid = 1745,
          visible = true,
          properties = {
            ["animated"] = false
          }
        },
        {
          id = 13,
          name = "",
          type = "",
          shape = "rectangle",
          x = 240,
          y = 368,
          width = 32,
          height = 16,
          rotation = -270,
          gid = 1745,
          visible = true,
          properties = {
            ["animated"] = false
          }
        },
        {
          id = 14,
          name = "",
          type = "",
          shape = "rectangle",
          x = 240,
          y = 440,
          width = 64,
          height = 64,
          rotation = 0,
          gid = 504,
          visible = true,
          properties = {
            ["animated"] = false
          }
        },
        {
          id = 16,
          name = "",
          type = "",
          shape = "rectangle",
          x = 320,
          y = 416,
          width = 64,
          height = 64,
          rotation = 0,
          gid = 503,
          visible = true,
          properties = {
            ["animated"] = false
          }
        },
        {
          id = 17,
          name = "",
          type = "",
          shape = "rectangle",
          x = 240,
          y = 464,
          width = 32,
          height = 16,
          rotation = -270,
          gid = 1745,
          visible = true,
          properties = {
            ["animated"] = false
          }
        },
        {
          id = 18,
          name = "",
          type = "",
          shape = "rectangle",
          x = 136,
          y = 32,
          width = 64,
          height = 64,
          rotation = 0,
          gid = 583,
          visible = true,
          properties = {
            ["animated"] = false
          }
        },
        {
          id = 19,
          name = "",
          type = "",
          shape = "rectangle",
          x = 152,
          y = 216,
          width = 64,
          height = 64,
          rotation = 0,
          gid = 57,
          visible = true,
          properties = {
            ["animated"] = false
          }
        },
        {
          id = 20,
          name = "",
          type = "",
          shape = "rectangle",
          x = 216,
          y = 216,
          width = 32,
          height = 16,
          rotation = 0,
          gid = 1745,
          visible = true,
          properties = {}
        },
        {
          id = 21,
          name = "",
          type = "",
          shape = "rectangle",
          x = 248,
          y = 216,
          width = 32,
          height = 16,
          rotation = 0,
          gid = 1745,
          visible = true,
          properties = {}
        },
        {
          id = 22,
          name = "",
          type = "",
          shape = "rectangle",
          x = 280,
          y = 216,
          width = 32,
          height = 16,
          rotation = 0,
          gid = 1745,
          visible = true,
          properties = {}
        },
        {
          id = 23,
          name = "",
          type = "",
          shape = "rectangle",
          x = 312,
          y = 216,
          width = 32,
          height = 16,
          rotation = 0,
          gid = 1745,
          visible = true,
          properties = {}
        },
        {
          id = 24,
          name = "",
          type = "",
          shape = "rectangle",
          x = 184,
          y = 216,
          width = 32,
          height = 16,
          rotation = 0,
          gid = 1743,
          visible = true,
          properties = {}
        },
        {
          id = 25,
          name = "",
          type = "",
          shape = "rectangle",
          x = 136,
          y = 72,
          width = 16,
          height = 16,
          rotation = 0,
          gid = 1748,
          visible = true,
          properties = {}
        },
        {
          id = 26,
          name = "",
          type = "",
          shape = "rectangle",
          x = 112,
          y = 104,
          width = 16,
          height = 16,
          rotation = 0,
          gid = 1748,
          visible = true,
          properties = {}
        },
        {
          id = 27,
          name = "",
          type = "",
          shape = "rectangle",
          x = 136,
          y = 120,
          width = 16,
          height = 16,
          rotation = 0,
          gid = 1748,
          visible = true,
          properties = {}
        },
        {
          id = 28,
          name = "",
          type = "",
          shape = "rectangle",
          x = 160,
          y = 104,
          width = 16,
          height = 16,
          rotation = 0,
          gid = 1748,
          visible = true,
          properties = {}
        },
        {
          id = 29,
          name = "",
          type = "",
          shape = "rectangle",
          x = 160,
          y = 104,
          width = 16,
          height = 16,
          rotation = 0,
          gid = 1748,
          visible = true,
          properties = {}
        }
      }
    },
    {
      type = "objectgroup",
      name = "hud",
      visible = true,
      opacity = 1,
      offsetx = 0,
      offsety = 0,
      draworder = "topdown",
      properties = {},
      objects = {
        {
          id = 3,
          name = "",
          type = "",
          shape = "text",
          x = 32,
          y = 560,
          width = 192,
          height = 48,
          rotation = 0,
          visible = true,
          text = "SCORE\n    00015050",
          fontfamily = "PCPaint Bold Medium",
          wrap = true,
          color = { 255, 255, 255 },
          properties = {
            ["textfont"] = "font/pcpaintboldmedium.fnt"
          }
        },
        {
          id = 4,
          name = "",
          type = "",
          shape = "text",
          x = 224,
          y = 560,
          width = 224,
          height = 48,
          rotation = 0,
          visible = true,
          text = "More HUD here if I need",
          fontfamily = "PCPaint Bold Medium",
          wrap = true,
          color = { 255, 255, 255 },
          halign = "right",
          properties = {
            ["textfont"] = "font/pcpaintboldmedium.fnt"
          }
        }
      }
    }
  }
}
