<?xml version="1.0" encoding="UTF-8"?>
<tileset name="spider01" tilewidth="64" tileheight="64" tilecount="50" columns="10">
 <tileoffset x="-32" y="32"/>
 <image source="spider01.png" width="640" height="320"/>
 <tile id="0" type="Enemy">
  <properties>
   <property name="faceangle" type="float" value="270"/>
   <property name="name" value="attacknorth"/>
  </properties>
  <objectgroup draworder="index">
   <object id="1" x="16" y="16" width="32" height="32">
    <properties>
     <property name="collidable" type="bool" value="true"/>
     <property name="sensor" type="bool" value="true"/>
    </properties>
   </object>
  </objectgroup>
  <animation>
   <frame tileid="0" duration="100"/>
   <frame tileid="1" duration="100"/>
   <frame tileid="2" duration="100"/>
   <frame tileid="3" duration="100"/>
  </animation>
 </tile>
 <tile id="1" type="Enemy">
  <properties>
   <property name="faceangle" type="float" value="270"/>
  </properties>
 </tile>
 <tile id="2" type="Enemy">
  <properties>
   <property name="faceangle" type="float" value="270"/>
  </properties>
 </tile>
 <tile id="3" type="Enemy">
  <properties>
   <property name="faceangle" type="float" value="270"/>
  </properties>
 </tile>
 <tile id="4" type="Enemy">
  <properties>
   <property name="faceangle" type="float" value="270"/>
   <property name="name" value="walknorth"/>
  </properties>
  <objectgroup draworder="index">
   <object id="1" x="16" y="16" width="32" height="32">
    <properties>
     <property name="collidable" type="bool" value="true"/>
     <property name="sensor" type="bool" value="true"/>
    </properties>
   </object>
  </objectgroup>
  <animation>
   <frame tileid="4" duration="100"/>
   <frame tileid="5" duration="100"/>
   <frame tileid="6" duration="100"/>
   <frame tileid="7" duration="100"/>
   <frame tileid="8" duration="100"/>
   <frame tileid="9" duration="100"/>
  </animation>
 </tile>
 <tile id="5" type="Enemy">
  <properties>
   <property name="faceangle" type="float" value="270"/>
  </properties>
 </tile>
 <tile id="6" type="Enemy">
  <properties>
   <property name="faceangle" type="float" value="270"/>
  </properties>
 </tile>
 <tile id="7" type="Enemy">
  <properties>
   <property name="faceangle" type="float" value="270"/>
  </properties>
 </tile>
 <tile id="8" type="Enemy">
  <properties>
   <property name="faceangle" type="float" value="270"/>
  </properties>
 </tile>
 <tile id="9">
  <properties>
   <property name="faceangle" type="float" value="270"/>
  </properties>
 </tile>
 <tile id="10" type="Enemy">
  <properties>
   <property name="faceangle" type="float" value="180"/>
   <property name="name" value="attackwest"/>
  </properties>
  <objectgroup draworder="index">
   <object id="1" x="16" y="16" width="32" height="32">
    <properties>
     <property name="collidable" type="bool" value="true"/>
     <property name="sensor" type="bool" value="true"/>
    </properties>
   </object>
  </objectgroup>
  <animation>
   <frame tileid="10" duration="100"/>
   <frame tileid="11" duration="100"/>
   <frame tileid="12" duration="100"/>
   <frame tileid="13" duration="100"/>
  </animation>
 </tile>
 <tile id="11" type="Enemy">
  <properties>
   <property name="faceangle" type="float" value="180"/>
  </properties>
 </tile>
 <tile id="12" type="Enemy">
  <properties>
   <property name="faceangle" type="float" value="180"/>
  </properties>
 </tile>
 <tile id="13" type="Enemy">
  <properties>
   <property name="faceangle" type="float" value="180"/>
  </properties>
 </tile>
 <tile id="14" type="Enemy">
  <properties>
   <property name="faceangle" type="float" value="180"/>
   <property name="name" value="walkwest"/>
  </properties>
  <objectgroup draworder="index">
   <object id="1" x="16" y="16" width="32" height="32">
    <properties>
     <property name="collidable" type="bool" value="true"/>
     <property name="sensor" type="bool" value="true"/>
    </properties>
   </object>
  </objectgroup>
  <animation>
   <frame tileid="14" duration="100"/>
   <frame tileid="15" duration="100"/>
   <frame tileid="16" duration="100"/>
   <frame tileid="17" duration="100"/>
   <frame tileid="18" duration="100"/>
   <frame tileid="19" duration="100"/>
  </animation>
 </tile>
 <tile id="15" type="Enemy">
  <properties>
   <property name="faceangle" type="float" value="180"/>
  </properties>
 </tile>
 <tile id="16" type="Enemy">
  <properties>
   <property name="faceangle" type="float" value="180"/>
  </properties>
 </tile>
 <tile id="17" type="Enemy">
  <properties>
   <property name="faceangle" type="float" value="180"/>
  </properties>
 </tile>
 <tile id="18" type="Enemy">
  <properties>
   <property name="faceangle" type="float" value="180"/>
  </properties>
 </tile>
 <tile id="19">
  <properties>
   <property name="faceangle" type="float" value="180"/>
  </properties>
 </tile>
 <tile id="20" type="Enemy">
  <properties>
   <property name="faceangle" type="float" value="90"/>
   <property name="name" value="attacksouth"/>
  </properties>
  <objectgroup draworder="index">
   <object id="1" x="16" y="16" width="32" height="32">
    <properties>
     <property name="collidable" type="bool" value="true"/>
     <property name="sensor" type="bool" value="true"/>
    </properties>
   </object>
  </objectgroup>
  <animation>
   <frame tileid="20" duration="100"/>
   <frame tileid="21" duration="100"/>
   <frame tileid="22" duration="100"/>
   <frame tileid="23" duration="100"/>
  </animation>
 </tile>
 <tile id="21" type="Enemy">
  <properties>
   <property name="faceangle" type="float" value="90"/>
  </properties>
 </tile>
 <tile id="22" type="Enemy">
  <properties>
   <property name="faceangle" type="float" value="90"/>
  </properties>
 </tile>
 <tile id="23" type="Enemy">
  <properties>
   <property name="faceangle" type="float" value="90"/>
  </properties>
 </tile>
 <tile id="24" type="Enemy">
  <properties>
   <property name="faceangle" type="float" value="90"/>
   <property name="name" value="walksouth"/>
  </properties>
  <objectgroup draworder="index">
   <object id="1" x="16" y="16" width="32" height="32">
    <properties>
     <property name="collidable" type="bool" value="true"/>
     <property name="sensor" type="bool" value="true"/>
    </properties>
   </object>
  </objectgroup>
  <animation>
   <frame tileid="24" duration="100"/>
   <frame tileid="25" duration="100"/>
   <frame tileid="26" duration="100"/>
   <frame tileid="27" duration="100"/>
   <frame tileid="28" duration="100"/>
   <frame tileid="29" duration="100"/>
  </animation>
 </tile>
 <tile id="25" type="Enemy">
  <properties>
   <property name="faceangle" type="float" value="90"/>
  </properties>
 </tile>
 <tile id="26" type="Enemy">
  <properties>
   <property name="faceangle" type="float" value="90"/>
  </properties>
 </tile>
 <tile id="27" type="Enemy">
  <properties>
   <property name="faceangle" type="float" value="90"/>
  </properties>
 </tile>
 <tile id="28" type="Enemy">
  <properties>
   <property name="faceangle" type="float" value="90"/>
  </properties>
 </tile>
 <tile id="29">
  <properties>
   <property name="faceangle" type="float" value="90"/>
  </properties>
 </tile>
 <tile id="30" type="Enemy">
  <properties>
   <property name="faceangle" type="float" value="0"/>
   <property name="name" value="attackeast"/>
  </properties>
  <objectgroup draworder="index">
   <object id="1" x="16" y="16" width="32" height="32">
    <properties>
     <property name="collidable" type="bool" value="true"/>
     <property name="sensor" type="bool" value="true"/>
    </properties>
   </object>
  </objectgroup>
  <animation>
   <frame tileid="30" duration="100"/>
   <frame tileid="31" duration="100"/>
   <frame tileid="32" duration="100"/>
   <frame tileid="33" duration="100"/>
  </animation>
 </tile>
 <tile id="31" type="Enemy">
  <properties>
   <property name="faceangle" type="float" value="0"/>
  </properties>
 </tile>
 <tile id="32" type="Enemy">
  <properties>
   <property name="faceangle" type="float" value="0"/>
  </properties>
 </tile>
 <tile id="33" type="Enemy">
  <properties>
   <property name="faceangle" type="float" value="0"/>
  </properties>
 </tile>
 <tile id="34" type="Enemy">
  <properties>
   <property name="faceangle" type="float" value="0"/>
   <property name="name" value="walkeast"/>
  </properties>
  <objectgroup draworder="index">
   <object id="1" x="16" y="16" width="32" height="32">
    <properties>
     <property name="collidable" type="bool" value="true"/>
     <property name="sensor" type="bool" value="true"/>
    </properties>
   </object>
  </objectgroup>
  <animation>
   <frame tileid="34" duration="100"/>
   <frame tileid="35" duration="100"/>
   <frame tileid="36" duration="100"/>
   <frame tileid="37" duration="100"/>
   <frame tileid="38" duration="100"/>
   <frame tileid="39" duration="100"/>
  </animation>
 </tile>
 <tile id="35" type="Enemy">
  <properties>
   <property name="faceangle" type="float" value="0"/>
  </properties>
 </tile>
 <tile id="36" type="Enemy">
  <properties>
   <property name="faceangle" type="float" value="0"/>
  </properties>
 </tile>
 <tile id="37" type="Enemy">
  <properties>
   <property name="faceangle" type="float" value="0"/>
  </properties>
 </tile>
 <tile id="38" type="Enemy">
  <properties>
   <property name="faceangle" type="float" value="0"/>
  </properties>
 </tile>
 <tile id="39">
  <properties>
   <property name="faceangle" type="float" value="0"/>
  </properties>
 </tile>
 <tile id="40" type="Enemy">
  <properties>
   <property name="animenddiscard" type="bool" value="true"/>
   <property name="collidable" type="bool" value="false"/>
   <property name="name" value="die"/>
  </properties>
  <animation>
   <frame tileid="40" duration="33"/>
   <frame tileid="41" duration="33"/>
   <frame tileid="42" duration="33"/>
   <frame tileid="43" duration="33"/>
   <frame tileid="44" duration="33"/>
   <frame tileid="43" duration="33"/>
   <frame tileid="44" duration="33"/>
   <frame tileid="43" duration="33"/>
   <frame tileid="44" duration="33"/>
   <frame tileid="43" duration="33"/>
   <frame tileid="44" duration="33"/>
   <frame tileid="43" duration="33"/>
   <frame tileid="44" duration="33"/>
   <frame tileid="43" duration="33"/>
  </animation>
 </tile>
 <tile id="41" type="Enemy">
  <properties>
   <property name="animenddiscard" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="42" type="Enemy">
  <properties>
   <property name="animenddiscard" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="43" type="Enemy">
  <properties>
   <property name="animenddiscard" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="44" type="Enemy"/>
 <tile id="45" type="Enemy"/>
 <tile id="46" type="Enemy"/>
 <tile id="47" type="Enemy"/>
 <tile id="48" type="Enemy"/>
</tileset>
