local levity = require "levity"

local math_pi = math.pi
local math_huge = math.huge
local math_max = math.max
local math_min = math.min
local math_rad = math.rad
local math_cos = math.cos
local math_sin = math.sin

local Enemy = class()
function Enemy:_init(enemy)
	self.enemy = enemy
	local properties = enemy.properties

	self.health = properties.health or 1

	local body = enemy.body
	local x1, y1, x2, y2 = math_huge, math_huge, -math_huge, -math_huge
	for _, fixture in pairs(body:getFixtureList()) do
		local fx1, fy1, fx2, fy2 = fixture:getBoundingBox()
		x1 = math_min(x1, fx1)
		y1 = math_min(y1, fy1)
		x2 = math_max(x2, fx2)
		y2 = math_max(y2, fy2)
	end
	self.halfw = (x2 - x1)/2
	self.halfh = (y2 - y1)/2
end

function Enemy:beginMove(dt)
	if self.dead then
		return
	end
	local enemy = self.enemy
	local properties = enemy.properties
	local faceangle = math_rad(properties.faceangle or 0)
	if properties.movebackward then
		faceangle = faceangle + math_pi
	end
	local movex = math_cos(faceangle)
	local movey = math_sin(faceangle)

	local movespeed
	if self.safeareaid then
		movespeed = properties.moveslowspeed or 20
	else
		movespeed = properties.movespeed or 60
	end
	movespeed = movespeed + levity.scripts:call(levity.mapfile, "getEnemySpeedInc")
	local vx1, vy1 = movex*movespeed, movey*movespeed

	local body = enemy.body
	local vx0, vy0 = body:getLinearVelocity()
	local mass = body:getMass()
	body:applyLinearImpulse(mass*(vx1 - vx0), mass*(vy1 - vy0))
end

function Enemy:beginContact(myfixture, otherfixture, contact)
	local otherdata = otherfixture:getBody():getUserData()
	if otherdata then
		local enemy = self.enemy
		local properties = enemy.properties
		local otherobject = otherdata.object
		local otherproperties = otherdata.properties

		levity.scripts:send(otherdata.id, "hitSomething", true)

		local hitdamage = otherproperties.hitdamage
		if hitdamage then
			levity.bank:play(otherproperties.hitdamagesound)
			self.health = self.health - hitdamage
			if self.health <= 0 then
				self:killEnemy()
			end
		end
	end
end

function Enemy:killEnemy()
	if not self.dead then
		self.dead = true
		local enemy = self.enemy
		local properties = enemy.properties
		levity.bank:play(properties.diesound)
		enemy.body:setLinearVelocity(0, 0)
		levity.scripts:send(levity.mapfile, "enemyKilled")
	end
end

function Enemy:endMove(dt)
	local enemy = self.enemy
	local properties = enemy.properties
	if self.dead then
		local tileid = properties.dietileid or "die"
		local gid = levity.map:getTileGid(enemy.tile.tileset, tileid)
		if enemy.gid ~= gid then
			enemy:setTileId(tileid)
		end
	end
	if not self.dead then
		local faceangle = math_rad(properties.faceangle or 0)
		local grabx = math_cos(faceangle)
		local graby = math_sin(faceangle)
		local body = enemy.body
		local x, y = body:getPosition()

		local safeareaid = levity.scripts:call("void", "getSafeArea",
							x + grabx*self.halfw,
							y + graby*self.halfh)
		local safearea = safeareaid and levity.map.objects[safeareaid]
		if safearea then
			local vx, vy = body:getLinearVelocity()
			local dtvx = vx*dt
			local dtvy = vy*dt

			if dtvx > 0 then
				safearea.x = safearea.x + dtvx
				safearea.width = safearea.width - dtvx
			elseif dtvx < 0 then
				safearea.width = safearea.width + dtvx
			end

			if dtvy > 0 then
				safearea.y = safearea.y + dtvy
				safearea.height = safearea.height - dtvy
			elseif dtvy < 0 then
				safearea.height = safearea.height + dtvy
			end
		end
		self.safeareaid = safeareaid
	end
end
--[[
function Enemy:endDraw()
	local enemy = self.enemy
	local properties = enemy.properties
	local faceangle = math_rad(properties.faceangle or 0)
	local grabx = math_cos(faceangle)
	local graby = math_sin(faceangle)
	love.graphics.points(enemy.x + grabx*self.halfw, enemy.y + graby*self.halfh)
end
]]
return Enemy
