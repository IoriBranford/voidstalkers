local levity = require "levity"
local string_format = string.format
local math_pi = math.pi
local math_cos = math.cos
local math_sin = math.sin
local math_max = math.max
local math_min = math.min
local love_math_random = love.math.random

local Game = class()
function Game:_init(map)
	self.properties = map.properties

	local world = levity.world
	for _, body in pairs(world:getBodyList()) do
		body:setFixedRotation(true)
		for _, fixture in pairs(body:getFixtureList()) do
			fixture:setFriction(0)
		end
	end
	for _, objecttype in pairs(map.objecttypes) do
		for k, v in pairs(objecttype) do
			if levity.bank.isAudioFile(v) then
				levity.bank:load(v)
			end
		end
	end
	for k, v in pairs(self.properties) do
		if levity.bank.isAudioFile(v) then
			levity.bank:load(v)
		end
	end

	self.player = map.objects[map.properties.playerid]

	local width = map.width*map.tilewidth
	local height = map.height*map.tileheight
	levity.camera:set(width/2, height/2, width, height)
	local music = map.properties.music
	if music then
		levity.bank:load(music, "emu")
	end

	self.enemywave = 0

	self.enemyspawndist = self.properties.enemyspawndist
	self.enemyspawntime = self.properties.enemyspawntime
	self.enemykillsfornextwave = self.properties.enemykillsfornextwave

	self.enemyspawntimer = self.properties.nextwavewaittime
	self.enemykills = 0

	self.growsafeareatime = 0

	self.hud = {}
	for _, object in pairs(map.layers["hud"].objects) do
		self.hud[object.name] = object
	end
end

function Game:gameStart()
	if self.enemywave < 1 then
		self:newWave()
		levity.bank:play(self.properties.music)
	end
end

function Game:newWave()
	if self.enemywave < self.properties.maxwave then
		levity.scripts:broadcast("killEnemy")
		levity.bank:play(self.properties.nextwavesound)
		self.enemywave = self.enemywave + 1
		--self.enemyspawndist = self.enemyspawndist +
		--	(self.properties.enemyspawndistinc or -20)
		self.enemyspawntime = self.properties.enemyspawntime +
			(self.properties.enemyspawntimeinc or -.25)*self.enemywave
		self.enemykillsfornextwave = self.properties.enemykillsfornextwave +
			(self.properties.enemykillsfornextwaveinc or 5)*self.enemywave
		self.growsafeareatime = self.properties.nextwavegrowsafeareatime
	end
	self.enemyspawntimer = self.enemyspawntime + self.properties.nextwavewaittime
	self.enemykills = 0
	self:updateHud()
end

function Game:enemyKilled()
	self.enemykills = self.enemykills + 1
	self:updateHud()
end

function Game:updateHud()
	local hudmessage = self.hud.message
	hudmessage.text = string.format("Wave %d\n%d/%d", self.enemywave,
		self.enemykills, self.enemykillsfornextwave)
end

local Dirs = { "east", "south", "west", "north" }
local DirAngle = {
	["east"]=0,
	["south"]=math_pi*.5,
	["west"]=math_pi*1,
	["north"]=math_pi*1.5
}

local function newEnemy(enemywave, spawndist)
	local dir = Dirs[love_math_random(4)]
	local x, y = levity.scripts:call("void", "randomSafeAreaPoint")
	local tileset = string_format("spider%02d", love_math_random(enemywave))
	local tileid = "attack"..dir
	local angle = DirAngle[dir]
	x = x + math_cos(angle) * spawndist
	y = y + math_sin(angle) * spawndist
	local layer = levity.map.layers["enemies"]
	local gid = levity.map:getTileGid(tileset, tileid)
	local enemy = {
		gid = gid,
		x = x,
		y = y
	}
	layer:addObject(enemy)
end

function Game:getEnemySpeedInc()
	return self.enemywave*(self.properties.enemyspeedinc or 10)
end

function Game:beginMove(dt)
	if self.enemykills >= self.enemykillsfornextwave then
		self:newWave()
	end

	if self.enemywave > 0 then
		local growsafeareatime = self.growsafeareatime
		if growsafeareatime > 0 then
			local growth = math_min(dt, growsafeareatime)
			self.growsafeareatime = growsafeareatime - growth
			levity.scripts:send("void", "growSafeAreas", growth)
		end

		local enemyspawntimer = self.enemyspawntimer
		enemyspawntimer = enemyspawntimer - dt
		if enemyspawntimer <= 0 then
			enemyspawntimer = enemyspawntimer + self.enemyspawntime
			newEnemy(self.enemywave, self.enemyspawndist)
		end
		self.enemyspawntimer = enemyspawntimer
	end
end

function Game:beginContact(myfixture, otherfixture, contact)
	local otherdata = otherfixture:getBody():getUserData()
	if otherdata then
		levity.scripts:send(otherdata.id, "hitSomething", false)
	end
end

function Game:gameOver()
	local music = levity.bank.currentmusic
	if music then
		music:fade()
	end
	local hudmessage = self.hud.message
	hudmessage.text = "F2=restart\nESC=quit"
end

function Game:keypressed_f2()
	levity:setNextMap(levity.mapfile)
end

function Game:keypressed_escape()
	love.event.quit()
end

function Game:keypressed_s()
	local filename = os.date("shot_%Y-%m-%d_%H-%M-%S")
	if love.filesystem.exists(filename) then
		for i = 1, 999 do
			local newfilename = filename..'_'..i
			if not love.filesystem.exists(newfilename) then
				filename = newfilename
				break
			end
		end
	end

	local screenshotdata = love.graphics.newScreenshot()
	screenshotdata:encode("png", filename..".png")
end

return Game
