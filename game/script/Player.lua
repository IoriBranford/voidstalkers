local levity = require "levity"
local math_hypotsq = math.hypotsq
local math_huge = math.huge
local math_abs = math.abs
local math_max = math.max
local math_min = math.min
local math_deg = math.deg
local math_cos = math.cos
local math_sin = math.sin
local math_atan2 = math.atan2
local love_physics_newBody = love.physics.newBody
local love_graphics_rectangle = love.graphics.rectangle

local Player = class()
function Player:_init(object)
	self.player = object
	self.infirex, self.infirey = 0, 1
end

local love_joystick_getJoysticks = love.joystick.getJoysticks
local love_keyboard_isDown = love.keyboard.isDown

local AxisDeadZoneSq = 0.25*0.25

local function getInput()
	local inx, iny, infire, infirestrafe = 0, 0, false, false

	local joysticks = love_joystick_getJoysticks()
	for i = 1, #joysticks do
		local joystick = joysticks[i]
		if joystick:isGamepad() then
			inx = joystick:getGamepadAxis("leftx")
			iny = joystick:getGamepadAxis("lefty")
			if math_hypotsq(inx, iny) < AxisDeadZoneSq then
				inx = 0
				iny = 0
			end
			if joystick:isGamepadDown("dpleft") then
				inx = inx - 1
			end
			if joystick:isGamepadDown("dpright") then
				inx = inx + 1
			end
			if joystick:isGamepadDown("dpup") then
				iny = iny - 1
			end
			if joystick:isGamepadDown("dpdown") then
				iny = iny + 1
			end
			infire = infire or joystick:isGamepadDown("a")
			infirestrafe = infirestrafe or joystick:isGamepadDown("b")
		else
			inx = joystick:getAxis(1)
			iny = joystick:getAxis(2)
			if math_hypotsq(inx, iny) < AxisDeadZoneSq then
				inx = 0
				iny = 0
			end
			for i = 1, joystick:getHatCount() do
				local hat = joystick:getHat(i)
				if hat:find("l") then
					inx = inx - 1
				elseif hat:find("r") then
					inx = inx + 1
				end
				if hat:find("u") then
					iny = iny - 1
				elseif hat:find("d") then
					iny = iny + 1
				end
			end

			infire = infire or joystick:isDown(1)
			infirestrafe = infirestrafe or joystick:isDown(2)
		end
	end

	if love_keyboard_isDown("left") then
		inx = inx - 1
	end
	if love_keyboard_isDown("right") then
		inx = inx + 1
	end
	if love_keyboard_isDown("up") then
		iny = iny - 1
	end
	if love_keyboard_isDown("down") then
		iny = iny + 1
	end
	infire = infire or love_keyboard_isDown("z")
	infirestrafe = infirestrafe or love_keyboard_isDown("x")

	return inx, iny, infire, infirestrafe
end

local function move(player, inx, iny)
	if not player then return end
	local properties = player.properties
	local movespeed = properties.movespeed or 120
	local body = player.body
	local vx0, vy0 = body:getLinearVelocity()
	local vx1, vy1 = inx*movespeed, iny*movespeed
	local mass = body:getMass()
	body:applyLinearImpulse(mass*(vx1 - vx0), mass*(vy1 - vy0))
end

local function newBullet(gid, shooterbody, speed, angle, layer)
	local world = shooterbody:getWorld()
	local x, y = shooterbody:getPosition()
	local vx = speed*math_cos(angle)
	local vy = speed*math_sin(angle)

	local body = love_physics_newBody(world, x, y, "dynamic")
	body:setLinearVelocity(vx, vy)
	body:setAngle(angle)
	body:setBullet(true)

	local bullet = {
		gid = gid,
		body = body,
		x = x,
		y = y,
		rotation = math_deg(angle)
	}

	layer:addObject(bullet)
end

local function fire(player, angle)
	local properties = player.properties

	local bullettileset = properties.bullettileset or "playershot"
	local bullettileid = properties.bullettileid or 0
	local gid = levity.map:getTileGid(bullettileset, bullettileid)
	local body = player.body
	local bulletspeed = properties.bulletspeed or 600
	local layer = player.layer
	newBullet(gid, body, bulletspeed, angle, layer)

	levity.bank:play(properties.bulletsound)
end

local AngleDir = {
	[0] = "east",
	[90] = "south",
	[180] = "west",
	[270] = "north",
}

function Player:beginMove(dt)
	local player = self.player
	local properties = player.properties

	local inx, iny, infire, infirestrafe = getInput()
	if inx ~= 0 or iny ~= 0 or infire or infirestrafe then
		levity.scripts:send(levity.mapfile, "gameStart")
	end

	local infirex, infirey = self.infirex, self.infirey
	if not infirestrafe and (inx ~= 0 or iny ~= 0) then
		infirex = inx
		infirey = iny
		self.infirex = infirex
		self.infirey = infirey
	end
	local absfirex = math_abs(infirex)
	local absfirey = math_abs(infirey)

	local gid = player.gid
	if self.dead then
		gid = levity.map:getTileGid("die", 0)
	elseif inx == 0 and iny == 0 then
		local dir = AngleDir[properties.faceangle] or "south"
		if infire or infirestrafe then
			gid = levity.map:getTileGid("swing", dir)
		else
			gid = levity.map:getTileGid("walk", "stand"..dir)
		end
	elseif absfirex < absfirey then
		if infirey < 0 then
			gid = levity.map:getTileGid("walk", "walknorth")
		elseif infirey > 0 then
			gid = levity.map:getTileGid("walk", "walksouth")
		end
	else
		if infirex < 0 then
			gid = levity.map:getTileGid("walk", "walkwest")
		elseif infirex > 0 then
			gid = levity.map:getTileGid("walk", "walkeast")
		end
	end

	if gid ~= player.gid then
		player:setGid(gid, nil, nil, false)
	end

	local bullettimer = properties.bullettimer or 0
	bullettimer = math_max(0, bullettimer - dt)
	if not self.dead then
		move(player, inx, iny)

		infire = infire or infirestrafe
		if infire and bullettimer <= 0 then
			local angle = math_atan2(infirey, infirex)
			fire(player, angle)
			bullettimer = bullettimer + (properties.bullettime or 0.1)
		end
	end
	properties.bullettimer = bullettimer
end

local function kill(self)
	if not self.dead then
		local player = self.player
		local properties = player.properties
		self.dead = true
		levity.bank:play(properties.diesound)
		player.body:setLinearVelocity(0, 0)
		local gid = levity.map:getTileGid("die", 0)
		player:setGid(gid, nil, nil, false)
	end
end

function Player:beginContact(myfixture, otherfixture, contact)
	local otherdata = otherfixture:getBody():getUserData()
	if otherdata then
		local otherobject = otherdata.object
		local otherproperties = otherdata.properties

		if otherproperties.killplayer and not self.shieldtime then
			kill(self)
		end
	end
end

function Player:endMove(dt)
	local player = self.player
	local body = player.body
	if not levity.scripts:call("void", "isBodyFullyInSafeArea", body) then
		kill(self)
	end
	if self.shieldtime then
		self.shieldtime = self.shieldtime - dt
		if self.shieldtime <= 0 then
			self.shieldtime = nil
		end
	end
end

local function tryRespawn(self)
	local player = self.player
	local x1, y1, x2, y2 = math_huge, math_huge,
	-math_huge, -math_huge
	for _, fixture in pairs(player.body:getFixtureList()) do
		local fx1, fy1, fx2, fy2 = fixture:getBoundingBox()
		x1 = math_min(x1, fx1)
		y1 = math_min(y1, fy1)
		x2 = math_max(x2, fx2)
		y2 = math_max(y2, fy2)
	end

	local x, y = levity.scripts:call("void", "findSafeSpawn", x2-x1, y2-y1)
	if x and y then
		self.dead = false
		player.body:setPosition(x, y)
		self.shieldtime = player.properties.respawnshieldtime or 3
		return true
	end
	return false
end

function Player:loopedAnimation()
	if self.dead then
		if not tryRespawn(self) then
			levity:discardObject(self.player.id)
			levity.scripts:broadcast("gameOver")
		end
	end
end

function Player:beginDraw()
	if self.shieldtime then
		love.graphics.setColor(255,255,255,
			256*math.sin(30*math.pi*love.timer.getTime()))
	end
end

function Player:endDraw()
	love.graphics.setColor(255,255,255)
end

return Player
