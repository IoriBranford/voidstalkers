local levity = require "levity"
local math_abs = math.abs
local math_max = math.max
local math_min = math.min

local SafeArea = class()
function SafeArea:_init(object)
	self.safearea = object
end

function SafeArea:getSafeSpawn(minw, minh)
	local safearea = self.safearea
	local sax = safearea.x
	local say = safearea.y
	local saw = safearea.width
	local sah = safearea.height
	if saw > minw and sah > minh then
		return sax + saw/2, say + sah/2
	end
end

function SafeArea:testPoint(x, y)
	local safearea = self.safearea
	local x1 = safearea.x
	local y1 = safearea.y
	local x2 = safearea.width + x1
	local y2 = safearea.height + y1
	return x1 < x and x < x2 and y1 < y and y < y2
end

function SafeArea:testBounds(x1, y1, x2, y2)
	local safearea = self.safearea
	local sax1 = safearea.x
	local say1 = safearea.y
	local sax2 = safearea.width + sax1
	local say2 = safearea.height + say1
	if x2 < sax1 or sax2 < x1 or y2 < say1 or say2 < y1 then
		return "outside"
	end
	if x1 < sax1 or sax2 < x2 or y1 < say1 or say2 < y2 then
		return "edge"
	end
	return "inside"
end

function SafeArea:grow(dt, limitx1, limity1, limitx2, limity2)
	local safearea = self.safearea
	local growth = (safearea.properties.growspeed or 60) * dt
	local x1 = safearea.x - growth
	local y1 = safearea.y - growth
	local x2 = x1 + safearea.width + 2*growth
	local y2 = y1 + safearea.height + 2*growth
	x1 = math_max(x1, limitx1)
	y1 = math_max(y1, limity1)
	x2 = math_min(x2, limitx2)
	y2 = math_min(y2, limity2)
	safearea.x      = x1
	safearea.y      = y1
	safearea.width  = x2 - x1
	safearea.height = y2 - y1
end

return SafeArea
