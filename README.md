# VoidStalkers

The voidstalkers are bringing the void down on you. Hold out as long as you can.

## How to Play

Shoot voidstalkers to stop them pulling the void over the stage. The void stops your bullets. Kill the number shown below to advance to the next wave.

You die when touching a voidstalker or the void. Infinite lives, but game over when there is not enough space to respawn.

## Controls

Use keyboard or controller.

- Arrow keys, left analog, D-pad = move
- Z key, A button, 1st button = fire; hold to strafe (maintain direction)

## Credits

- Design, code, sound: Iori Branford
- Graphics:
	- Liberated Pixel Cup (LPC) Base Assets (sprites & map tiles) https://opengameart.org/content/liberated-pixel-cup-lpc-base-assets-sprites-map-tiles
	- Redshrike and William.Thompsonj
		- [LPC] Spider https://opengameart.org/content/lpc-spider
	- Sharm
		- [LPC] Animated water and fire https://opengameart.org/content/lpc-animated-water-and-fire
		- [LPC] Dungeon Elements https://opengameart.org/content/lpc-dungeon-elements
	- Johann C
		- [LPC] A shoot'em up complete graphic kit
	- Player character made from https://github.com/jrconway3/Universal-LPC-spritesheet
	- codeman38
		- PC Paint Bold font http://www.zone38.net/font/
- Music:
	- Toshiaki Sakoda
		- Devil Crash MD "Main Table" http://project2612.org/details.php?id=19
